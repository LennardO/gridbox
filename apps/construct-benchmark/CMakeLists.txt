add_executable(construct-benchmark
        construct-main.cpp
        ${BACKWARD_ENABLE} include/ConstructionBenchmark.h src/ConstructionBenchmark.cpp)
target_compile_options(construct-benchmark PRIVATE -Werror=return-type)
add_backward(construct-benchmark)
target_link_libraries(construct-benchmark PUBLIC bfd)
target_include_directories(construct-benchmark PUBLIC include)
target_link_libraries(construct-benchmark PUBLIC algorithms benchmark)