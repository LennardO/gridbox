//
// Created by lennard on 8/1/20.
//

#pragma once

#include "Aggregate.h"
#include <algorithms/interfaces/GridSearch.h>

namespace gridbox {

  struct construction_bm_result {
    u_short maxRegionSize;
    std::chrono::nanoseconds constructionTime;
    u_short height;
    std::vector<fp_t> fillRates;
    std::vector<fp_t> relativeIncrease;
    fp_t absoluteIncrease;
    benchmark_double utilization;
    u_long memoryUsage;
  };

  template<GridSearchC GridSearch>
  construction_bm_result constructionBenchmark(GridSearch&                        gridSearch,
                                               const typename GridSearch::Grid&   grid,
                                               u_short                            maxRegionSize) {
    auto vSizeBefore = ownVSizeInBytes();
    auto t0 = std::chrono::steady_clock::now();
    construct(gridSearch, grid, maxRegionSize);
    auto t1 = std::chrono::steady_clock::now();
    auto vSizeAfter = ownVSizeInBytes();

    construction_bm_result result;

    result.maxRegionSize = maxRegionSize;
    result.constructionTime = std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0);
    result.height = height(gridSearch.fmtree());
    result.fillRates = fillRate(gridSearch.fmtree());
    result.relativeIncrease = primitiveIncrease(gridSearch.fmtree());
    auto leaves = allLeaves(gridSearch.fmtree());
    for(auto const& leaf : leaves)
      result.utilization += ((double)region(leaf)->size()) / maxRegionSize;
    u_int numPrimitives = 0;
    for(auto const& leaf : leaves)
      numPrimitives += region(leaf)->size();
    result.absoluteIncrease = (double) numPrimitives / primitives(grid).size()  -1.0;
    result.memoryUsage = vSizeAfter < vSizeBefore ? 0 : vSizeAfter - vSizeBefore;

    return result;
  }

  std::ostream& operator<<(std::ostream& o, const construction_bm_result& bmResult);

  void addMeansDistinctToFiles(const construction_bm_result& bmResult, const std::filesystem::path& dirPath);
}