
#include <algorithms/implementations/d3/OctreeTetraGridSearch.h>
#include <algorithms/functions/RegionSearch.h>

INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv) {
  using namespace gridbox;

  el::Configurations defaultConf;
  defaultConf.setGlobally(el::ConfigurationType::Format, "%level: %msg");
  defaultConf.set(el::Level::Trace, el::ConfigurationType::Enabled, "false");
  el::Loggers::reconfigureLogger("default", defaultConf);

  if(argc < 5){
    LOG(ERROR) << "not enough arguments";
    LOG(INFO) << "usage: ./generate-rays <GridSearchFile> <out RaysFile> <N Origins> <N Directions>";
    return -1;
  }
  std::string gsFilePath = argv[1];
  std::filesystem::path outFilePath = argv[2];
  int nOrigins = std::stoi(argv[3]);
  int nDirections = std::stoi(argv[4]);

  OctreeTetraGridSearch gridSearch;
  loadFromFile(gridSearch, gsFilePath);

  using GridSearch = OctreeTetraGridSearch;
  using AABox = typename GridSearch::AABox;
  using Point = typename AABox::Point;
  using Vector = typename Point::Vector;
  using Ray = typename GridSearch::Ray;
  using Sphere = typename AABox::Sphere;

  auto box = aabb(gridSearch);
  fp_t rd = norm2(midPoint(box)-minPoint(box)) + 2*EPSILON;
  Point ct = midPoint(box);
  Sphere sphere = {ct, rd};

  std::vector<Ray> result;
  for(int o=0; o<nOrigins; o++) {
    Point origin = ct + rd * random<Vector>();
    for(int d=0; d<nDirections; d++) {
      bool rayFound = false;
      while(!rayFound) {
        Ray ray = {origin, random<Vector>()};
        auto regions = getIntersectedBy(gridSearch, ray);
        if( !intersectedPrimitives(regions, ray).empty() ) {
          result.emplace_back(ray);
          rayFound = true;
        }
      }
    }
  }

  std::ofstream ofs(outFilePath);
  if(!ofs)
    throw std::runtime_error("failed to open file " + outFilePath.string());

  for(auto const& ray : result)
    ofs << toJson(ray).dump() << '\n';

  return 0;
}
