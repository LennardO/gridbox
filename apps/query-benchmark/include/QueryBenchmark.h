//
// Created by lennard on 7/19/20.
//

#pragma once

#include "Aggregate.h"

#include <algorithms/interfaces/GridSearch.h>
#include <algorithms/implementations/FullMAryTreeGridSearch.h>
#include <algorithms/implementations/LinearGridSearch.h>
#include <algorithms/functions/RegionSearch.h>

#include <cpucounters.h>

namespace gridbox {

  bool initQueryBenchmark();
  void cleanupQueryBenchmark();

  struct query_bm_result {
    u_short maxRegionSize;
    benchmark_time queryTime;
    benchmark_double meanQueryUtilization;
    benchmark_double resultHitRate;
    benchmark_bytes bytesRead;
    benchmark_double hitRatioL2;
    benchmark_double hitRatioL3;
    benchmark_uint64 missesL2;
    benchmark_uint64 missesL3;
  };

  template<GridSearchC GridSearch>
  query_bm_result queryBenchmark(const GridSearch& gridSearch, const std::filesystem::path& raysFilePath) {
    using namespace pcm;

    if(!initQueryBenchmark())
      throw std::runtime_error("initialization of the query benchmark failed");

    query_bm_result result;
    std::vector<typename GridSearch::Ray> rays;
    std::ifstream ifs(raysFilePath);
    if(!ifs)
      throw std::runtime_error("failed to open file " + raysFilePath.string());
    std::string line;
    while (std::getline(ifs, line)) {
      json jl = json::parse(line);
      rays.emplace_back(fromJson<typename GridSearch::Ray>(jl));
    }

    auto maxRS = maxRegionSize(gridSearch);
    result.maxRegionSize = maxRS;

    for(auto const& ray : rays) {
      SystemCounterState before = getSystemCounterState();
      auto t0 = std::chrono::steady_clock::now();
      auto regions = getIntersectedBy(gridSearch, ray);
      auto t1 = std::chrono::steady_clock::now();
      SystemCounterState after = getSystemCounterState();

      result.queryTime += std::chrono::duration_cast<std::chrono::nanoseconds>(t1-t0);
      auto rSizes = regionSizes(regions);
      result.meanQueryUtilization += ((double)rSizes.sum()/rSizes.size()) / maxRS;
      result.resultHitRate += (double) intersectedPrimitives(regions, ray).size() / (double) rSizes.sum();
      result.bytesRead += getBytesReadFromMC(before, after);
      result.hitRatioL2 += getL2CacheHitRatio(before, after);
      result.hitRatioL3 += getL3CacheHitRatio(before, after);
      result.missesL2 += getL2CacheMisses(before, after);
      result.missesL3 += getL3CacheMisses(before, after);
    }
    cleanupQueryBenchmark();
    return result;
  }

  std::ostream& operator<<(std::ostream& o, const query_bm_result& bmResult);

  void addMeansDistinctToFiles(const query_bm_result& bmResult, const std::filesystem::path& dirPath);
}
