//
// Created by lennard on 8/1/20.
//

#include "QueryBenchmark.h"

namespace gridbox {
  bool initQueryBenchmark() {
    using namespace pcm;

    PCM* m = PCM::getInstance();
    auto errCode = m->program();
    if (errCode != PCM::Success){
      if(errCode == PCM::MSRAccessDenied)
        LOG(ERROR) << "MSRAccessDenied";
      else if(errCode == PCM::PMUBusy)
        LOG(ERROR) << "PMUBusy";
      else
        LOG(ERROR) << "UnknownError";
      m->cleanup();
      return false;
    }
    return true;
  }
  void cleanupQueryBenchmark() {
    pcm::PCM::getInstance()->cleanup();
  }

  std::ostream& operator<<(std::ostream& o, const query_bm_result& bmResult) {
    o << "benchmark result with mrs=" + std::to_string(bmResult.maxRegionSize) + ":\n";
    o << " queryTime: " << bmResult.queryTime << '\n';
    o << " meanQueryUtilization: " << bmResult.meanQueryUtilization << '\n';
    o << " memoryRead: " << bmResult.bytesRead << '\n';
    o << " resultHitRate: " << bmResult.resultHitRate << '\n';
    o << " hitRatioL2: " << bmResult.hitRatioL2 << '\n';
    o << " missesL2: " << bmResult.missesL2 << '\n';
    o << " hitRatioL3: " << bmResult.hitRatioL3 << '\n';
    o << " missesL3: " << bmResult.missesL3 << '\n';
    return o;
  }


  void addMeansDistinctToFiles(const query_bm_result& bmResult, const std::filesystem::path& dirPath) {
    std::filesystem::create_directories(dirPath);
    std::string id = std::to_string(bmResult.maxRegionSize);
    addDistinctResultToFile(id, std::to_string(bmResult.queryTime.mean.count()), dirPath / "queryTime");
    addDistinctResultToFile(id, std::to_string(bmResult.meanQueryUtilization.mean), dirPath / "meanQueryUtilization");
    addDistinctResultToFile(id, std::to_string(bmResult.bytesRead.mean), dirPath / "memoryRead");
    addDistinctResultToFile(id, std::to_string(bmResult.resultHitRate.mean), dirPath / "resultHitRate");
    addDistinctResultToFile(id, std::to_string(bmResult.hitRatioL2.mean), dirPath / "hitRatioL2");
    addDistinctResultToFile(id, std::to_string(bmResult.hitRatioL3.mean), dirPath / "hitRatioL3");
    addDistinctResultToFile(id, std::to_string(bmResult.missesL2.mean), dirPath / "missesL2");
    addDistinctResultToFile(id, std::to_string(bmResult.missesL3.mean), dirPath / "missesL3");
  }
}