
find_package(OpenMP REQUIRED)
find_package(glfw3 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)

add_executable(render3D src/main.cpp ${BACKWARD_ENABLE} include/ThreadRenderer.h src/ThreadRenderer.cpp include/OpenglWindow.h src/OpenglWindow.cpp include/OpenglBuffer.h src/OpenglBuffer.cpp)
target_include_directories(render3D PRIVATE include)
target_compile_features(render3D PUBLIC cxx_std_20)
target_compile_options(render3D PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
add_backward(render3D)
target_link_libraries(render3D PRIVATE glfw OpenGL::GL GLEW::GLEW)
target_link_libraries(render3D PRIVATE algorithms)