//
// Created by lennard on 10/14/20.
//
#pragma once

#include <easylogging++.h>

#include <GL/glew.h> // should be included before GLFW
#include <GLFW/glfw3.h>

namespace gridbox {
  class OpenglBuffer {
  public:
    OpenglBuffer();
    OpenglBuffer(const uint& width, const uint& height);
    ~OpenglBuffer();
    void create();
    [[nodiscard]] GLuint id() const;

    uint width() const;

    uint height() const;

    void drawToFrame();
  private:
    GLuint _id;
    uint _width, _height;
  };
}