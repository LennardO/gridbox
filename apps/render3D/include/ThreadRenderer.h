//
// Created by lennard on 10/14/20.
//
#pragma once

#include <algorithms/interfaces/GridSearch.h>
#include "OpenglBuffer.h"


namespace gridbox {

  struct PixelRGBA {
    u_char r;
    u_char g;
    u_char b;
    u_char a;
  };

  class ThreadRenderer {
  public:
    void setOutputBuffer(const OpenglBuffer& openglBuffer);

#pragma clang diagnostic push
#pragma ide diagnostic ignored "openmp-use-default-none"
    template<GridSearchC GridSearch>
    void render(const GridSearch& gridSearch, const std::vector<typename GridSearch::Ray>& rays){
      auto texPtr = (PixelRGBA*) glMapNamedBuffer(_openglBuffer.id(), GL_WRITE_ONLY);
      if (texPtr == nullptr) {
        LOG(FATAL) << "failed to map buffer";
        exit(-1);
      }

      uint width = _openglBuffer.width();
      uint height = _openglBuffer.height();
      fp_t sumMax = 10000;

//      #pragma omp parallel for default(none) shared(width,height,gridSearch,rays,sumMax,texPtr)
      #pragma omp parallel for
      for(uint y=0; y<height; y++) {
        LOG(INFO) << "line " << y;
        for (uint x = 0; x < width; x++) {
          auto regions = getIntersectedBy(gridSearch, rays[y * width + x]);
          fp_t dataSum = 0;
          bool maxReached = false;
          for (auto const& region : regions) {
            if(maxReached) break;
            for (auto const& tetra : region) {
              if(maxReached) break;
              if (intersect(rays[y * width + x], tetra)) {
                dataSum += data(tetra);
                if(dataSum >= sumMax) maxReached = true;
              }
            }
          }
          fp_t intensity = dataSum / sumMax;
          intensity = intensity > 1 ? 1 : intensity;
          texPtr[y * width + x] = {255, (u_char) (255 - intensity * 255), (u_char) (255 - intensity * 255), 255};
        }
      }

      if( ! glUnmapNamedBuffer(_openglBuffer.id()) )
        LOG(WARNING) << "failed to unmap buffer";
    }
#pragma clang diagnostic pop

  private:
    OpenglBuffer _openglBuffer;
  };
}