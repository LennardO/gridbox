//
// Created by lennard on 10/14/20.
//
#include "ThreadRenderer.h"

namespace gridbox {

  void ThreadRenderer::setOutputBuffer(const OpenglBuffer& openglBuffer) {
    _openglBuffer = openglBuffer;
  }
}