//
// Created by lennard on 6/19/20.
//

#pragma once

#include <QtWidgets/QGraphicsLineItem>
#include <cmath>

namespace gridbox {
  class Ray2DItem : public QGraphicsLineItem {
  public:
    using Ray2D = Ray2DWithPrecomputation;

    Ray2DItem(const Ray2D& ray, const QRectF& sceneRect) {
      QLineF line;
      qreal xOrigin = 100.0 * ray.origin()[0];
      qreal yOrigin = -100.0 * ray.origin()[1];
      qreal xDirection = 100.0 * ray.direction()[0];
      qreal yDirection = -100.0 * ray.direction()[1];

      qreal xT = INFINITY;
      if (xDirection > 0)
        xT = calcT(xOrigin, xDirection, sceneRect.right());
      else if (xDirection < 0)
        xT = calcT(xOrigin, xDirection, sceneRect.left());

      qreal yT = INFINITY;
      if (yDirection > 0)
        yT = calcT(yOrigin, yDirection, sceneRect.bottom());
      else if (yDirection < 0)
        yT = calcT(yOrigin, yDirection, sceneRect.top());

      qreal minT = xT < yT ? xT : yT;
      qreal xDestination = xOrigin + minT * xDirection;
      qreal yDestination = yOrigin + minT * yDirection;

      line.setLine(xOrigin, yOrigin, xDestination, yDestination);
      setLine(line);
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
      QPen pen(Qt::blue, 5);
      painter->setPen(pen);
      painter->drawLine(line());
      pen.setWidth(10);
      painter->setPen(pen);
      painter->drawPoint(line().p1());
    }

  private:
    qreal calcT(qreal a, qreal b, qreal c) const {
      return (c - a) / b;
    }
  };
}