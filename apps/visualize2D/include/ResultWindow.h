//
// Created by lennard on 6/10/20.
//

#pragma once

#include "Window.h"
#include <geometry/implementations/d2/AABox2DByMinMax.h>
#include <grid/implementations/d2/TriangleGrid.h>

namespace gridbox {
  class ResultWindow : public Window {
  public:
    using TriangleRegion = typename TriangleGrid::Region;
    using TriangleRegions = std::vector<TriangleRegion>;

    void draw(const TriangleGrid& grid);
    void draw(const Ray2DWithPrecomputation& ray);
    void draw(const TriangleRegion& region);
    void draw(const TriangleRegions& regions);
    void draw(const AABox2DByMinMax& aab);

    void fitScene();

  private:
    QRectF _viewRect;
  };
}


