#pragma once

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

namespace gridbox {
  class Window : public QMainWindow {
  Q_OBJECT

  public:
    Window(QWidget* parent = nullptr);
    ~Window();

  protected:
    Ui::Window* ui;
    QGraphicsView* view;
    QGraphicsScene* scene;
  };
}
