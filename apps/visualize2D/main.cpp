


#include <QApplication>
#include <easylogging++.h>

#include <grid/implementations/d2/TriangleGrid.h>
#include <algorithms/implementations/LinearGridSearch.h>
#include <algorithms/implementations/FullMAryTreeGridSearch.h>
#include <algorithms/functions/RegionSearch.h>
#include "ResultWindow.h"

INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv) {
  using namespace gridbox;

  el::Configurations defaultConf;
  defaultConf.setGlobally(el::ConfigurationType::Format, "%level: %msg");
  defaultConf.set(el::Level::Trace, el::ConfigurationType::Enabled, "false");
  el::Loggers::reconfigureLogger("default", defaultConf);

  QApplication app(argc, argv);
  ResultWindow window;
  window.show();

  TriangleGrid grid2D;
//  loadTestData(grid2D);
  grid2D.loadUniformTestData();

  Ray2DWithPrecomputation ray2D({1, -0.5}, {1, 0.8});

  FullMAryTreeGridSearch<TriangleGrid> nts2D;
  construct(nts2D, grid2D, 6);
  FullMAryTreeGridSearch<TriangleGrid>::Regions quadResult = getIntersectedBy(nts2D, ray2D);

  for(auto const& aabb : allGeoRepresentations(nts2D.fmtree()))
    window.draw(aabb);

  window.draw(grid2D);
  window.draw(quadResult);
  window.draw(ray2D);

  return app.exec();
}
