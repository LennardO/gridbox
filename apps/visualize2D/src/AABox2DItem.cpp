//
// Created by lennard on 6/19/20.
//

#include "AABox2DItem.h"

using gridbox::AABox2DItem;

AABox2DItem::AABox2DItem(const AABox2D& aab){
  QRectF rec;
  rec.setCoords(100.0*aab.min()[0], -100.0*aab.min()[1], 100.0*aab.max()[0], -100.0*aab.max()[1]);
  setRect(rec);
}

AABox2DItem::AABox2DItem(const AABox2D& aab, const QPen& pen)
: AABox2DItem(aab)
{
  _pen = pen;
}

void AABox2DItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
  painter->setPen(_pen);
  painter->drawRect(rect());
}