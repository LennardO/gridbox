#include "Window.h"
#include "./ui_window.h"

using gridbox::Window;

Window::Window(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::Window) {
  ui->setupUi(this);

  scene = new QGraphicsScene(this);
  view = ui->graphicsView;
  view->setScene(scene);
  view->setCacheMode(QGraphicsView::CacheNone);
}

Window::~Window() {
  delete ui;
  delete scene;
}
