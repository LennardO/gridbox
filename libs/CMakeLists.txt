
add_subdirectory(core)
add_subdirectory(geometry)
add_subdirectory(grid)
add_subdirectory(algorithms)

add_subdirectory(benchmark)