//
// Created by lennard on 7/24/20.
//

#pragma once

#include "algorithms/interfaces/FullMAryTree.h"

namespace gridbox {

  template<FullMAryTreeC FMTree>
  inline bool isLeaf(const FMTree& fmtree) {
    return region(fmtree) != nullptr;
  }

  template<FullMAryTreeC FMTree>
  inline u_short height(const FMTree& fmTree) {
    return fillRate(fmTree).size() -1;
  }

  template<FullMAryTreeC FMTree>
  inline std::vector<fp_t> fillRate(const FMTree& fmTree) {
    auto arity = FMTree::Arity;
    std::vector<fp_t> result;
    std::vector<typename FMTree::FMTreePtr> nodes = { std::make_shared<FMTree>(fmTree) };
    for(u_short i=0; ! nodes.empty(); i++) {
      fp_t rate = nodes.size() / std::pow(arity, i);
      result.emplace_back(rate);
      std::vector<typename FMTree::FMTreePtr> childNodes;
      for(auto const& node : nodes)
        if( ! isLeaf(*node) )
          for(auto const& childNode : children(*node))
            childNodes.emplace_back(childNode);
      nodes = childNodes;
    }
    return result;
  }

  template<FullMAryTreeC FMTree>
  inline std::vector<FMTree> allLeaves(const FMTree& fmTree) {
    if(isLeaf(fmTree))
      return { fmTree };
    std::vector<FMTree> result;
    for(auto const& child : children(fmTree))
      for(auto const& leaf : allLeaves(*child))
        result.emplace_back(leaf);
    return result;
  }

  template<FullMAryTreeC FMTree>
  inline std::vector<fp_t> primitiveIncrease(const FMTree& fmTree) {
    std::vector<fp_t> result;
    std::vector<FMTree> nodes = { fmTree };
    while( !nodes.empty() ) {
      u_long numPrimitives = 0;
      for(auto const& node : nodes)
        if( !isLeaf(node))
          numPrimitives += node.initialNumPrimitives;
      std::vector<FMTree> childNodes;
      for(auto const& node : nodes)
        if( !isLeaf(node))
          for(auto const& childNode : children(node))
            childNodes.emplace_back(*childNode);
      u_long numChildPrimitives = 0;
      for(auto const& childNode : childNodes)
        numChildPrimitives += childNode.initialNumPrimitives;
      if(numChildPrimitives != 0)
        result.emplace_back((fp_t)numChildPrimitives / numPrimitives  -1.0);

      nodes = childNodes;
    }
    return result;
  }
}