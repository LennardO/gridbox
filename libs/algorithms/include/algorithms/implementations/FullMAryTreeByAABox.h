//
// Created by lennard on 7/24/20.
//

#pragma once

#include "algorithms/functions/FullMAryTree.h"
#include "geometry/implementations/AABoxByMinMax.h"
#include "grid/functions/Region.h"
#include "geometry/functions/Coordinates.h"
#include <cassert>
#include <omp.h>
#include <numeric>

namespace gridbox {

  DEFINE_CONCEPT(FullMAryTreeByAABox, FullMAryTree);

  template<RegionC RegionT, AABoxC AABoxT>
  class FullMaryTreeByAABox {
  public:
    using Concept = CFullMAryTreeByAABox;
    using Region = RegionT;
    using AABox = AABoxT;
    using GeoType = AABox;
    using Primitive = typename Region::Primitive;
    using Ray = typename Primitive::Ray;
    using FMTreePtr = std::shared_ptr<FullMaryTreeByAABox<Region,AABox>>;
    inline static const u_short Arity = AABox::num_orthants;

    std::shared_ptr<Region> region() const {
      return _region;
    }
    void setRegion(const Region& region) {
      _region = std::make_shared<Region>(region);
    }
    AABox box() const {
      return _box;
    }
    void setBox(const AABox& box) {
      _box = box;
    }
    std::array<FMTreePtr,Arity> children() const {
      assert(_children != nullptr);
      return *_children;
    };
    void setChildrenPtr(const std::shared_ptr<std::array<FMTreePtr,Arity>>& childrenPtr) {
      _children = childrenPtr;
    }

    u_long initialNumPrimitives;
  private:
    std::shared_ptr<Region> _region;
    AABox _box;
    std::shared_ptr<std::array<FMTreePtr,Arity>> _children;
  };

  template<FullMAryTreeByAABoxC FMTree>
  inline std::vector<std::shared_ptr<typename FMTree::Region>> intersectedRegions(const FMTree&                 fmtree,
                                                                                  const typename FMTree::Ray&   ray) {
    auto box = fmtree.box();
    if( ! intersect(ray, box))
      return {};
    if(isLeaf(fmtree))
      return { region(fmtree) };

    return intersectedChildRegions(fmtree, ray);
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline std::vector<std::shared_ptr<typename FMTree::Region>> intersectedChildRegions(const FMTree&                 fmtree,
                                                                                       const typename FMTree::Ray&   ray) {
    auto childs = children(fmtree);
    std::vector<std::pair<fp_t,u_short>> childLambdasAndIDs;
    for(u_short i=0; i<childs.size(); i++) {
      auto lambda = intersectionLambdaIn(ray, childs[i]->box());
      if(lambda)
        childLambdasAndIDs.emplace_back(std::make_pair(lambda.value(), i));
    }
    assert( ! childLambdasAndIDs.empty() );
    std::sort(childLambdasAndIDs.begin(), childLambdasAndIDs.end());//sorts by first element

    std::vector<std::shared_ptr<typename FMTree::Region>> result;
    for(auto const& lambdaID : childLambdasAndIDs) {
      auto child = *childs[lambdaID.second];
      if(isLeaf(child))
        result.emplace_back(region(child));
      else
        for(auto const& childRegion : intersectedChildRegions(child, ray))
          result.emplace_back(childRegion);
    }
    return result;
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline void constructTree(FMTree& fmtree, const std::vector<typename FMTree::Primitive>& primitives, const std::vector<size_t>& pIDs, const u_short& maxRegionSize) {
    auto box = fmtree.box();
    auto mid = midPoint(box);
    fmtree.initialNumPrimitives = pIDs.size();
    if(fmtree.initialNumPrimitives > 100000)
      LOG(INFO) << "node primitives: " << fmtree.initialNumPrimitives;

    if(pIDs.size() <= maxRegionSize) {
      LOG(TRACE) << "leaf " << box << " -> " << pIDs.size();
      fmtree.setRegion({});
      for(const size_t& i : pIDs)
        fmtree.region()->emplace_back(primitives[i]);
      return;
    }

    auto childBoxes = orthants(box);
    LOG(TRACE) << box << " -> " << pIDs.size();
    for(auto const& cb : childBoxes)
      LOG(TRACE) << "   " << cb;
    std::array<std::vector<size_t>, FMTree::Arity> childPIDs;
    std::vector<std::array<std::vector<size_t>, FMTree::Arity>> childPIDsByThread(omp_get_max_threads());
    #pragma omp parallel default(none) shared(pIDs,primitives,childPIDsByThread,childPIDs,childBoxes,mid)
    {
    #pragma omp for
    for(size_t i=0; i<pIDs.size(); i++) {
      u_short childID = 0;
      bool distinct = true;
      auto primitive = primitives[pIDs[i]];
      auto pts = points(primitive);
      for(u_short d=0; d<dimensions(pts[0]); d++) {
        std::array<bool,FMTree::Primitive::NumPoints> isLess;
        for(u_short j=0; j<pts.size(); j++)
          isLess[j] = pts[j][d] <= mid[d];
        if( ! std::equal(isLess.begin()+1, isLess.end(), isLess.begin()) ) {
          distinct = false;
          break;
        }
        if( !isLess[0] )
          childID += pow2(dimensions(pts[0]) -1 -d);
      }
      auto threadID = omp_get_thread_num();
      if(distinct) {
        childPIDsByThread[threadID][childID].emplace_back(pIDs[i]);
        continue;
      }

      for(u_short c=0; c<childBoxes.size(); c++)
        if(intersect(primitive, childBoxes[c]))
          childPIDsByThread[threadID][c].emplace_back(pIDs[i]);
    }
    #pragma omp for
    for(int c=0; c<childBoxes.size(); c++)
      for(u_short tID=0; tID<omp_get_num_threads(); tID++)
        for(auto const& id : childPIDsByThread[tID][c])
          childPIDs[c].emplace_back(id);
    };

    auto childrenPtr = std::make_shared<std::array<typename FMTree::FMTreePtr,FMTree::Arity>>();
    for(u_short i=0; i<FMTree::Arity; i++) {
      childrenPtr->at(i) = std::make_shared<FMTree>();
      childrenPtr->at(i)->setBox(childBoxes[i]);
      constructTree(*childrenPtr->at(i), primitives, childPIDs[i], maxRegionSize);
    }
    fmtree.setChildrenPtr(childrenPtr);
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline void constructTree(FMTree& fmtree, const std::vector<typename FMTree::Primitive>& primitives, const u_short& maxRegionSize) {
    auto box = aabb(primitives);
    fmtree.setBox(box);

    std::vector<size_t> ids(primitives.size());
    std::iota (std::begin(ids), std::end(ids), 0);

    LOG(DEBUG) << "max threads: " << omp_get_max_threads();
    constructTree(fmtree, primitives, ids, maxRegionSize);
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline std::shared_ptr<typename FMTree::Region> region(const FMTree& fmtree) {
    return fmtree.region();
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline std::array<typename FMTree::FMTreePtr,FMTree::Arity> children(const FMTree& fmtree) {
    return fmtree.children();
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline std::vector<typename FMTree::GeoType> allGeoRepresentations(const FMTree& fmTree) {
    std::vector<typename FMTree::GeoType> result = { fmTree.box() };
    if(isLeaf(fmTree))
      return result;
    for(auto const& child : children(fmTree))
      for(auto const& childBox : allGeoRepresentations(*child))
        result.emplace_back(childBox);
    return result;
  }

  template<AABoxC AABox>
  inline json toJson(const AABox& aab) {
    json result = {};
    result["min"] = toJson(minPoint(aab));
    result["max"] = toJson(maxPoint(aab));
    return result;
  }

  template<AABoxC AABox>
  inline AABox fromJson(const json& aabJSON) {
    using Point = typename AABox::Point;
    auto min = fromJson<Point>(aabJSON["min"]);
    auto max = fromJson<Point>(aabJSON["max"]);
    return AABox(min, max);
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline json toJson(const FMTree& fmTree) {
    json result = {};
    result["box"] = toJson(fmTree.box());
    LOG(INFO) << fmTree.box();
    if(isLeaf(fmTree)) {
      result["region"] = toJson(*fmTree.region());
      return result;
    }
    for(u_int i=0; i<FMTree::Arity; i++)
      addToJson(*children(fmTree)[i], result, i);
    return result;
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline void addToJson(const FMTree& fmTree, json& parentJSON, const u_int& ownID) {
    parentJSON["children"][ownID] = {};
    parentJSON["children"][ownID]["box"] = toJson(fmTree.box());
    LOG(INFO) << fmTree.box();
    if(isLeaf(fmTree)) {
      parentJSON["children"][ownID]["region"] = toJson(*fmTree.region());
      return;
    }
    for(u_int i=0; i<FMTree::Arity; i++)
      addToJson(*children(fmTree)[i], parentJSON["children"][ownID], i);
    return;
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline FMTree fromJson(const json& fmTreeJSON) {
    FMTree result;
    result.setBox(fromJson<typename FMTree::GeoType>(fmTreeJSON["box"]));
    if(fmTreeJSON.contains("region")) {
      result.setRegion(fromJson<typename FMTree::Region>(fmTreeJSON["region"]));
      return result;
    }

    auto childrenPtr = std::make_shared<std::array<typename FMTree::FMTreePtr,FMTree::Arity>>();
    json childrenJSON = fmTreeJSON["children"];
    for(u_short i=0; i<childrenJSON.size(); i++)
      childrenPtr->at(i) = std::make_shared<FMTree>(fromJson<FMTree>(childrenJSON.at(i)));
    result.setChildrenPtr(childrenPtr);

    return result;
  }
}