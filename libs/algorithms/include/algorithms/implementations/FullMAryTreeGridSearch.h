//
// Created by lennard on 6/30/20.
//

#pragma once

#include "FullMAryTreeByAABox.h"
#include "algorithms/interfaces/GridSearch.h"

namespace gridbox {

  DEFINE_CONCEPT(FullMAryTreeGridSearch, GridSearch);

  template<GridC GridT>
  class FullMAryTreeGridSearch {
  public:
    using Concept = CFullMAryTreeGridSearch;
    using Grid = GridT;
    using Primitive = typename Grid::Primitive;
    using Primitives = std::vector<Primitive>;
    using Region = typename Grid::Region;
    using Regions = std::vector<Region>;
    using Point = typename Primitive::Point;
    using Ray = typename Primitive::Ray;
    using AABB = typename Primitive::AABB;
    using AABox = typename Grid::AABox;
    using FMTree = FullMaryTreeByAABox<Region,AABB>;

    void construct(const Grid& grid, u_short maxRegionSize) {
      _maxRegionSize = maxRegionSize;
      constructTree(_fmtree, primitives(grid), maxRegionSize);
    }

    Regions getIntersectedBy(const Ray& ray) const {

      Regions result = {};

      for(auto const& regionPtr : intersectedRegions(_fmtree, ray))
        result.emplace_back(*regionPtr);

      return result;
    }

    FMTree fmtree() const {
      return _fmtree;
    }
    void setFmtree(const FMTree& fmtree) {
      _fmtree = fmtree;
    }

    u_short maxRegionSize() const {
      return _maxRegionSize;
    }
    void setMaxRegionSize(u_short mrs) {
      _maxRegionSize = mrs;
    }

  private:
    FMTree _fmtree;
    u_short _maxRegionSize;
  };

  template<FullMAryTreeGridSearchC FMTreeGridSearch>
  inline void construct(FMTreeGridSearch& gridSearch, const typename FMTreeGridSearch::Grid& grid, u_short maxRegionSize) {
    return gridSearch.construct(grid, maxRegionSize);
  };

  template<FullMAryTreeGridSearchC FMTreeGridSearch>
  inline std::vector<typename FMTreeGridSearch::Region> getIntersectedBy(const FMTreeGridSearch& gridSearch, const typename FMTreeGridSearch::Ray& ray) {
    return gridSearch.getIntersectedBy(ray);
  }

  template<FullMAryTreeGridSearchC FMTreeGridSearch>
  inline json toJson(const FMTreeGridSearch& gridSearch) {
    return {
      {"fmtree", toJson(*gridSearch.fmtree().children().at(0))},
      {"maxRegionSize", gridSearch.maxRegionSize()}
    };
  }

  template<FullMAryTreeGridSearchC FMTreeGridSearch>
  inline FMTreeGridSearch fromJson(const json& gridSearchJSON) {
    FMTreeGridSearch result;
    result.setFmtree(fromJson<typename FMTreeGridSearch::FMTree>(gridSearchJSON["fmtree"]));
    result.setMaxRegionSize(gridSearchJSON["maxRegionSize"].get<u_short>());
    return result;
  }

  template<FullMAryTreeByAABoxC FMTree>
  inline void saveToFile(const FMTree& fmTree, std::ofstream& ofs, long& lid) {
    json result = {};
    result["box"] = toJson(fmTree.box());
    if(isLeaf(fmTree)) {
      result["region"] = toJson(*fmTree.region());
      ofs << result.dump() << '\n';
      lid++;
      return;
    }
    json childIDs = json::array();
    for(auto const& child : children(fmTree)) {
      saveToFile(*child, ofs, lid);
      childIDs.emplace_back(lid);
    }
    result["children"] = childIDs;
    ofs << result.dump() << '\n';
    lid++;
  }

  template<FullMAryTreeGridSearchC FMTreeGridSearch>
  inline void saveToFile(const FMTreeGridSearch& gridSearch, const std::filesystem::path& filepath) {
    std::ofstream ofs(filepath);
    if(!ofs)
      throw std::runtime_error("failed to open file " + filepath.string());
    ofs << gridSearch.maxRegionSize() << '\n';

    long lid = -1;
    saveToFile(gridSearch.fmtree(), ofs, lid);
  }

  template<FullMAryTreeGridSearchC FMTreeGridSearch>
  inline void loadFromFile(FMTreeGridSearch& gridSearch, const std::filesystem::path& filepath) {
    using FMTree = typename FMTreeGridSearch::FMTree;
    using FMTreePtr = typename FMTree::FMTreePtr;
    std::ifstream ifs(filepath);
    if(!ifs)
      throw std::runtime_error("failed to open file " + filepath.string());
    std::string line;
    std::getline(ifs, line);
    gridSearch.setMaxRegionSize(std::stoul(line));

    std::vector<FMTreePtr> nodes;
    while (std::getline(ifs, line))
    {
      json jl = json::parse(line);
      auto node = std::make_shared<FMTree>();
      node->setBox(fromJson<typename FMTree::AABox>(jl["box"]));
      if(jl.contains("region"))
        node->setRegion(fromJson<typename FMTree::Region>(jl["region"]));
      else {
        auto childrenJSON = jl["children"];
        auto children = std::make_shared<std::array<FMTreePtr,FMTree::Arity>>();
        for(u_short i=0; i<childrenJSON.size(); i++)
          children->at(i) = nodes[childrenJSON[i].get<long>()];
        node->setChildrenPtr(children);
      }
      nodes.emplace_back(node);
    }
    gridSearch.setFmtree(**nodes.rbegin());
  }

  template<FullMAryTreeGridSearchC NTGridSearch>
  inline typename NTGridSearch::AABB aabb(const NTGridSearch& gridSearch) {
    return gridSearch.fmtree().box();
  }

  template<FullMAryTreeGridSearchC NTGridSearch>
  inline u_short maxRegionSize(const NTGridSearch& gridSearch) {
    return gridSearch.maxRegionSize();
  }
}