//
// Created by lennard on 7/23/20.
//

#pragma once

#include "geometry/interfaces/Ray.h"

namespace gridbox {

  DEFINE_CONCEPT(FullMAryTree);

  template<FullMAryTreeC FMTree>
  inline std::vector<std::shared_ptr<typename FMTree::Region>> intersectedRegions(const FMTree& fmtree, const typename FMTree::Ray& ray);

  template<FullMAryTreeC FMTree>
  inline std::vector<std::shared_ptr<typename FMTree::Region>> intersectedChildRegions(const FMTree& fmtree, const typename FMTree::Ray& ray);
  //assumes that fmtree is not a leaf (and fmtree is intersected by ray)?

  //todo: require FullMAryTree::Arity or FullMAryTree::M

  template<FullMAryTreeC FMTree>
  inline std::shared_ptr<typename FMTree::Region> region(const FMTree& fmtree);
  //return nullptr if fmtree is not a leaf

  template<FullMAryTreeC FMTree>
  inline bool isLeaf(const FMTree& fmtree);

  template<FullMAryTreeC FMTree>
  inline std::array<typename FMTree::FMTreePtr,FMTree::Arity> children(const FMTree& fmtree);

  template<FullMAryTreeC FMTree>
  inline void constructTree(FMTree& fmtree, const std::vector<typename FMTree::Primitive>& primitives);

  template<FullMAryTreeC FMTree>
  inline std::vector<typename FMTree::GeoType> allGeoRepresentations(const FMTree& fmTree);

  template<FullMAryTreeC FMTree>
  inline u_short height(const FMTree& fmTree);

  template<FullMAryTreeC FMTree>
  inline std::vector<fp_t> fillRate(const FMTree& fmTree);

  template<FullMAryTreeC FMTree>
  inline std::vector<FMTree> allLeaves(const FMTree& fmTree);

  template<FullMAryTreeC FMTree>
  inline std::vector<fp_t> primitiveIncrease(const FMTree& fmTree);
}