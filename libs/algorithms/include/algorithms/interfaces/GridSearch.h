//
// Created by lennard on 6/30/20.
//

#pragma once

#include "grid/interfaces/Grid.h"

namespace gridbox {

  DEFINE_CONCEPT(GridSearch);

  template<GridSearchC GridSearch>
  inline void construct(GridSearch& gridSearch, const typename GridSearch::Grid& grid, u_short maxRegionSize);

  template<GridSearchC GridSearch>
  inline typename GridSearch::Regions getIntersectedBy(const GridSearch& gridSearch, const typename GridSearch::Ray& ray);

  template<GridSearchC GridSearch>
  inline void loadFromFile(const GridSearch& gridSearch, const std::filesystem::path& filepath);

  template<GridSearchC GridSearch>
  inline void saveToFile(const GridSearch& gridSearch, const std::filesystem::path& filepath);

  template<GridSearchC GridSearch>
  inline typename GridSearch::AABB aabb(const GridSearch& gridSearch);

  template<GridSearchC GridSearch>
  inline u_short maxRegionSize(const GridSearch& gridSearch);
}