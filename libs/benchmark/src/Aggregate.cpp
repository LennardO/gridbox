//
// Created by lennard on 8/1/20.
//

#include "Aggregate.h"

namespace gridbox {
  std::chrono::nanoseconds median(const std::multiset<std::chrono::nanoseconds>& data)
  {
    if (data.empty())
      throw std::length_error("Cannot calculate median value for empty dataset");

    const size_t n = data.size();
    std::chrono::nanoseconds median;

    auto iter = data.cbegin();
    std::advance(iter, n / 2);

    if (n % 2 == 0) //even number of values
      median = (*iter + *--iter) / 2;    // data[n/2 - 1] AND data[n/2]
    else
      median = *iter;

    return median;
  }

  std::chrono::nanoseconds mean(const std::multiset<std::chrono::nanoseconds>& data) {
    return sum(data) / data.size();
  }

  std::chrono::nanoseconds sd(const std::multiset<std::chrono::nanoseconds>& data, const std::chrono::nanoseconds& mean) {
    long sumSquaredDeviations = 0;
    for(auto const& el : data)
      sumSquaredDeviations += (el.count()-mean.count()) * (el.count()-mean.count());
    long variance = sumSquaredDeviations / data.size();

    return std::chrono::nanoseconds( (long) std::sqrt(variance) );
  }

  void addDistinctResultToFile(const std::string& id, const std::string& value, const std::filesystem::path& filePath) {
    std::vector<std::vector<std::string>> lines;
    if(std::filesystem::exists(filePath))
      lines = readRowsFromFile(filePath);
    bool found = false;
    for(auto& line : lines)
      if(line[0] == id) {
        line[1] = value;
        found = true;
        break;
      }
    if(!found)
      lines.emplace_back(std::vector{id, value});

    std::ofstream ofs(filePath);
    if(!ofs)
      throw std::runtime_error("failed to open " + filePath.string());
    for(auto const& line : lines)
      ofs << line[0] << " " << line[1] << '\n';
  }
}