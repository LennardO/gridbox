
find_package(OpenMP REQUIRED)

add_library(core

        include/core/Helper.h
        include/core/Concepts.h
        include/core/Counters.h

        src/Concepts.cpp
        src/Counters.cpp
        src/Helper.cpp
        src/easylogging++.cc)

target_compile_options(core PRIVATE -Werror=return-type)
target_include_directories(core PUBLIC include)
target_compile_features(core PUBLIC cxx_std_20)
target_link_libraries(core PUBLIC Half OpenMP::OpenMP_CXX)