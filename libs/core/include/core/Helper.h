//
// Created by lennard on 7/19/20.
//

#pragma once

#include <string>
#include <set>
#include <vector>
#include <sstream>
#include <valarray>
#include <concepts>
#include <nlohmann/json.hpp>
#include <filesystem>
#include <random>
#include "easylogging++.h"

namespace gridbox {
  using json = nlohmann::json;

  template<typename T>
  std::vector<std::pair<T,T>> permutationPairs(const std::set<T>& lhs, const std::set<T>& rhs) {
    std::vector<std::pair<T,T>> result = {};
    for(const T& lhsEl : lhs)
      for(const T& rhsEl : rhs) {
        result.emplace_back(std::make_pair(lhsEl, rhsEl));
        result.emplace_back(std::make_pair(rhsEl, lhsEl));
      }
    return result;
  }

  constexpr u_short pow2(u_short exponent) {
    return (exponent == 0) ? 1 : (2 * pow2(exponent-1));
  }

  constexpr u_long pow(u_long base, u_long exponent) {
    if(exponent == 0)
      return 1;
    return base * pow(base, exponent -1);
  }

  template<typename T>
  concept HasStreamOperator = requires(std::ostream o, T a) {
    { o << a};
  };
  template<HasStreamOperator T>
  std::string streamToStr(const T& a) {
    std::ostringstream ss;
    ss << a;
    return ss.str();
  }

  template<typename T>
  concept Container = true; //todo: es müsste eigentlich ein gleichnamiges concept in der stl geben

  template<Container C>
  std::string toString(const C& c) {
    std::string result = "(";
    auto it = c.begin();
    if(!c.empty())
      result += std::to_string(*it++);
    for(;it!=c.end();it++)
      result += ", " + std::to_string(*it);
    result += ")";
    return result;
  }

  std::vector<std::string> splitIntoWords(const std::string& str);
  std::vector<std::string> readWordsFromFile(const std::string& fileName);
  std::vector<std::string> readLinesFromFile(const std::string& fileName);
  std::vector<std::vector<std::string>> readRowsFromFile(const std::string& fileName);

  ulong ownVSizeInBytes();

  std::string bytesToReadableStr(u_long bytes);

  std::ostream& operator<<(std::ostream& o, std::chrono::nanoseconds ns);

  float randomFloat01();
}
