//
// Created by lennard on 7/19/20.
//

#include "core/Concepts.h"

namespace gridbox {

  std::map<std::string, std::set<std::string>>& conceptBases() {
    static std::map<std::string, std::set<std::string>> cBases = {};
    return cBases;
  }

  bool addRelation(const std::string& cName, const std::set<std::string>& bases) {
    if(conceptBases().contains(cName))
      return false;
    conceptBases().emplace(cName, bases);
    return true;
  }

  std::set<std::string> uniqueConceptNames(const std::string& cName) {
    auto cBases = conceptBases();
    std::set<std::string> result;
    result.insert(cName);
    for(auto const& bName : cBases.at(cName))
      for(auto const& subName : uniqueConceptNames(bName))
        result.insert(subName);
    return result;
  }

}