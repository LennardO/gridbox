//
// Created by lennard on 7/27/20.
//

#include "core/Helper.h"
#include <fstream>

namespace gridbox {


  std::vector<std::string> splitIntoWords(const std::string& str) {
    std::istringstream iss(str);
    return {
      std::istream_iterator<std::string>{iss},
      std::istream_iterator<std::string>{}
    };
  }

  std::vector<std::string> readWordsFromFile(const std::string& fileName) {
    std::ifstream file(fileName);
    if(!file)
      throw std::runtime_error("could not read file" + fileName);

    std::vector<std::string> fileContents;
    std::copy(std::istream_iterator<std::string>(file),
              std::istream_iterator<std::string>(),
              std::back_inserter(fileContents));

    return fileContents;
  }

  std::vector<std::string> readLinesFromFile(const std::string& fileName) {
    std::ifstream file(fileName);
    if(!file)
      throw std::runtime_error("could not read file" + fileName);

    std::vector<std::string> fileContents;
    std::string line;
    while(std::getline(file, line))
      fileContents.emplace_back(line);

    return fileContents;
  }

  std::vector<std::vector<std::string>> readRowsFromFile(const std::string& fileName) {

    std::vector<std::vector<std::string>> result;
    for(const std::string& line : readLinesFromFile(fileName))
      result.emplace_back(splitIntoWords(line));
    return result;
  }

  ulong ownVSizeInBytes() {
    return std::stoul(readWordsFromFile("/proc/self/stat").at(22)); // https://man7.org/linux/man-pages/man5/proc.5.html
  }

  std::string bytesToReadableStr(u_long bytes) {

    auto tb = bytes / pow(1024, 4);
    bytes -= tb * pow(1024, 4);
    auto gb = bytes / pow(1024, 3);
    bytes -= gb * pow(1024, 3);
    auto mb = bytes / pow(1024, 2);
    bytes -= mb * pow(1024, 2);
    auto kb = bytes / 1024;
    bytes -= kb * 1024;

    std::string result;
    u_short n = 0;
    if(tb > 0) {
      result += std::to_string(tb) + "TiB ";
      n++;
    }
    if(n==1 || gb>0) {
      result += std::to_string(gb) + "GiB ";
      n++;
    }
    if(n==1 || mb>0) {
      result += std::to_string(mb) + "MiB ";
      n++;
    }
    if(n==1 || kb>0) {
      result += std::to_string(kb) + "KiB ";
      n++;
    }
    if(n<2) {
      result += std::to_string(bytes) + "B";
    }
    return result;
  }

  std::ostream& operator<<(std::ostream& o, std::chrono::nanoseconds ns) {

    auto m = std::chrono::duration_cast<std::chrono::minutes>(ns);
    ns -= m;
    auto s = std::chrono::duration_cast<std::chrono::seconds>(ns);
    ns -= s;
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(ns);
    ns -= ms;
    auto us = std::chrono::duration_cast<std::chrono::microseconds>(ns);
    ns -= us;

    if(m.count())
      o << m.count() << "m ";
    if(s.count())
      o << s.count() << "s ";
    if(ms.count())
      o << ms.count() << "ms ";
    o << us.count() << "us";

    return o;
  }

  float randomFloat01() {
    static std::uniform_real_distribution<float> unif(0.0,std::nextafter(1.0, std::numeric_limits<float>::max()));
    static std::random_device rd;
    static std::default_random_engine re(rd());
    return unif(re);
  }
}
