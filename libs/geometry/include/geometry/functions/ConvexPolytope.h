//
// Created by lennard on 6/29/20.
//

#pragma once

#include "PointSet.h"
#include "LineSegment.h"
#include "geometry/interfaces/ConvexPolytope.h"
#include "geometry/interfaces/Concepts.h"
#include <set>
#include <sstream>

namespace gridbox {

  template<ConvexPolytopeC ConvexPolytopeA, ConvexPolytopeC ConvexPolytopeB>
  inline bool intersect(const ConvexPolytopeA& convexPolyA, const ConvexPolytopeB& convexPolyB) {
    INCREMENT_OP(intersection, ConvexPolytopeA, ConvexPolytopeB);
    for(auto const& point : points(convexPolyA))
      if(containsInInterior(convexPolyB, point))
        return true;

    for(auto const& polyFacetB : facets(convexPolyB))
      if(intersect(polyFacetB, convexPolyA))
        return true;

    return false;
  }

  template<LineSegmentC LineSegment, ConvexPolytopeC ConvexPolytope>
  inline bool intersect(const LineSegment& ls, const ConvexPolytope& convexPoly) {
    auto lambdas = intersectionLambdasInclusive(lsRay(ls), convexPoly);
    return lambdas.size() == 2 && lambdas[0] < 1;
  }

  template<ConvexPolytopeC ConvexPolytope>
  inline bool contains(const ConvexPolytope& convexPolytope, const typename ConvexPolytope::Point& point) {
    for(auto const& facet : facets(convexPolytope))
      if( !isLeftOrOn(point, plane(facet)))
        return false;
    return true;
  }

  template<ConvexPolytopeC ConvexPolytope>
  inline bool containsInInterior(const ConvexPolytope& convexPolytope, const typename ConvexPolytope::Point& point) {
    for(auto const& facet : facets(convexPolytope))
      if( !isLeft(point, plane(facet)))
        return false;
    return true;
  }

  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionLambdas(const typename ConvexPolytope::Ray& ray, const ConvexPolytope& convexPolytope)
                                  -> std::vector<fp_t> {
    auto inclusiveLambdas = intersectionLambdasInclusive(ray, convexPolytope);
    return inclusiveLambdas.size() == 2 ? inclusiveLambdas : std::vector<fp_t>{};
  }

  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionLambdasInclusive(const typename ConvexPolytope::Ray& ray, const ConvexPolytope& convexPolytope)
                                           -> std::vector<fp_t> {
    RAY_INTERSECT(ConvexPolytope);
    std::set<fp_t> result;

    if(containsInInterior(convexPolytope, origin(ray)))
      result.insert(0);

    for(auto const& facet : facets(convexPolytope)) {
      auto facetIntersecLambdas = intersectionLambdasInclusive(ray, facet);
      if (facetIntersecLambdas.size())
        result.insert(*facetIntersecLambdas.begin());
    }

    return result.empty() ? std::vector<fp_t>{} : std::vector<fp_t>{result.begin(), result.end()};
  }

  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionLambdasInOut(const typename ConvexPolytope::Ray&   ray,
                                       const ConvexPolytope&                 convexPolytope)
                                       -> std::optional<std::pair<fp_t, fp_t>> {

    auto lambdas = intersectionLambdas(ray, convexPolytope);
    std::sort(lambdas.begin(), lambdas.end());
    if(lambdas.empty())
      return std::nullopt;
    return std::make_pair(lambdas[0], lambdas[1]);
  }

  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionPointsInOut(const typename ConvexPolytope::Ray&   ray,
                                      const ConvexPolytope&                 convexPolytope)
                          -> std::optional<std::pair<typename ConvexPolytope::Point, typename ConvexPolytope::Point>> {

    auto lambdas = intersectionLambdasInOut(ray, convexPolytope);
    if(lambdas)
      return std::make_pair(rayPoint(ray, lambdas->first), rayPoint(ray, lambdas->second));
    return std::nullopt;
  }

  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionLambdaIn(const typename ConvexPolytope::Ray&   ray,
                                   const ConvexPolytope&                 convexPolytope) -> std::optional<fp_t> {

    auto inOut = intersectionLambdasInOut(ray, convexPolytope);
    if(inOut)
      return inOut->first;
    return std::nullopt;
  }
  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionPointIn(const typename ConvexPolytope::Ray&   ray,
                                  const ConvexPolytope&                 convexPolytope)
                                  -> std::optional<typename ConvexPolytope::Point> {

    auto inLambda = intersectionLambdaIn(ray, convexPolytope);
    if(inLambda)
      return rayPoint(ray, inLambda.value());
    return std::nullopt;
  }
}