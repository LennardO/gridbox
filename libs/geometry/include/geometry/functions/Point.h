//
// Created by Lennard Ockenfels on 5/27/20.
//

#pragma once

#include "geometry/interfaces/Point.h"
#include "Vector.h"

namespace gridbox {

//  template<typename T>
//  concept PointC = requires(T p) {
//    { p.coordinates() } -> std::same_as<std::valarray<fp_t>>;
//  };

//  template<typename Point>
//  u_short dimensions(const Point& point);
//  template<typename Point>
//  inline std::valarray<fp_t> coordinates(const Point& point) {
//    return point.coordinates();
//  }
//  template<PointC Point>
//  inline bool operator==(const Point& lhs, const Point& rhs) {
//    return (lhs.coordinates() == rhs.coordinates()).min();
//  }
  template<PointC Point, VectorC Vector>
  Point operator+(const Point& lhs, const Vector& rhs) {
    std::valarray<fp_t> result = coordinates(lhs) + coordinates(rhs);
    return result;
  }
  template<PointC Point>
  typename Point::Vector operator-(const Point& lhs, const Point& rhs) {
    std::valarray<fp_t> result = coordinates(lhs) - coordinates(rhs);
    return result;
  }
  template<PointC Point>
  inline Point mid(const Point& p0, const Point& p1) {
    return { 0.5 * coordinates(p0) + 0.5 * coordinates(p1) };
  }

//  template<typename Point>
//  Point mid(const Point& p0, const Point& p1);
//  template<PointC Point>
//  inline std::ostream& operator<<(std::ostream& o, const Point& point) {
//    o << "(" << point.get(0);
//    for (u_short i = 1; i < point.dimensions(); i++)
//      o << ", " << point.get(i);
//    o << ")";
//    return o;
//  }

}