//
// Created by lennard on 6/24/20.
//

#pragma once

#include "geometry/interfaces/Vector.h"
#include "Coordinates.h"
#include <numeric>

namespace gridbox {

  template<VectorC Vector>
  static fp_t squaredSum(const Vector& vector) {
    auto add_square = [](fp_t prev_sum, fp_t new_val) {
      return prev_sum + new_val * new_val;
    };
    auto coords = coordinates(vector);
    return std::accumulate(std::begin(coords), std::end(coords), (fp_t)0.0, add_square);
  }

  template<VectorC Vector>
  inline bool parallel(const Vector& a, const Vector& b) {
    fp_t bigK = 0;
    for(u_short d=0; d<dimensions(a); d++) {
      if(get(a,d)==0)
        continue;
      fp_t k = get(b,d) / get(a,d);
      if(abs(k) > abs(bigK))
        bigK = k;
    }
    if(bigK == 0)
      return false;

    std::valarray<fp_t> diff = coordinates(b) - bigK * coordinates(a);
    return diff.min() > -EPSILON && diff.max() < EPSILON;
  }


  template<VectorC Vector>
  inline fp_t dotProduct(const Vector& a, const Vector& b) {
    auto coordA = coordinates(a);
    auto coordB = coordinates(b);

    return std::inner_product(std::begin(coordA), std::end(coordA), std::begin(coordB), (fp_t)0.0);
  }

//  template<VectorC Vector>
//  requires requires { Vector::DIMENSIONS == 3 || Vector::DIMENSIONS == 7; } -> true
//  Vector crossProduct(const Vector& lhs, const Vector& rhs);


  template<VectorC Vector>
  inline fp_t norm2(const Vector& a) {
    return std::sqrt(squaredSum(a));
  }

  template<VectorC Vector>
  Vector operator+(const Vector& lhs, const Vector& rhs) {
    std::valarray<fp_t> result = coordinates(lhs) + coordinates(rhs);
    return result;
  }
  template<VectorC Vector>
  Vector operator-(const Vector& lhs, const Vector& rhs) {
    std::valarray<fp_t> result = coordinates(lhs) - coordinates(rhs);
    return result;
  }
  template<VectorC Vector>
  bool operator<(const Vector& lhs, const Vector& rhs) {
    return squaredSum(lhs) < squaredSum(rhs);
  }
  template<VectorC Vector>
  bool operator>(const Vector& lhs, const Vector& rhs) {
    return squaredSum(lhs) > squaredSum(rhs);
  }
  template<VectorC Vector>
  Vector operator*(const fp_t& scalar, const Vector& vector) {
    std::valarray<fp_t> result = scalar * coordinates(vector);
    return result;
  }
  template<VectorC Vector>
  Vector operator/(const Vector& vector, const fp_t& scalar) {
    std::valarray<fp_t> result = coordinates(vector) / scalar;
    return result;
  }
}