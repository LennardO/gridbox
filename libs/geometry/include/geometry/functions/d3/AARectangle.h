//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/interfaces/d3/AARectangle.h"

namespace gridbox {

  template<AARectangleC AARectangle>
  inline fp_t fixedValue(const AARectangle& aaRec) {
    return minPoint(aaRec)[fixedDimension(aaRec)];
  }

  template<AARectangleC AARectangle>
  inline static auto intersectionLambdaPoint(const typename AARectangle::Ray&   ray,
                                             const AARectangle&                 aaRec,
                                             bool                               inclusive)
                                             -> std::optional<std::pair<fp_t, typename AARectangle::Point>> {
    using Point = typename AARectangle::Point;
    using Vector = typename AARectangle::Vector;

    Point rayOrigin = origin(ray);
    Vector rayDir = direction(ray);
    u_short fixedDim = fixedDimension(aaRec);
    fp_t fixedVal = fixedValue(aaRec);
    Point min = minPoint(aaRec);
    Point max = maxPoint(aaRec);

    if(rayDir[fixedDim] == 0)
      return std::nullopt;

    std::pair<fp_t, Point> result;
    result.first = (fixedVal - rayOrigin[fixedDim]) / rayDir[fixedDim];
    if( result.first < -EPSILON)
      return std::nullopt;
    result.second = rayOrigin + result.first * rayDir;
    for(u_short d=1; d<Point::DIMENSIONS; d++) {
      u_short idx = (fixedDim + d) % Point::DIMENSIONS;
      if (   result.second[idx] < min[idx]
          || result.second[idx] > max[idx])
        return std::nullopt;
      if(!inclusive && (   result.second[idx] == min[idx]
                        || result.second[idx] == max[idx]))
        return std::nullopt;
    }

    return result;
  }

  template<AARectangleC AARectangle>
  inline auto intersectionLambdaPoint(const typename AARectangle::Ray& ray, const AARectangle& aaRec)
                                      -> std::optional<std::pair<fp_t, typename AARectangle::Point>> {
    return intersectionLambdaPoint(ray, aaRec, false);
  }

  template<AARectangleC AARectangle>
  inline auto intersectionLambdaPointInclusive(const typename AARectangle::Ray& ray, const AARectangle& aaRec)
                                               -> std::optional<std::pair<fp_t, typename AARectangle::Point>>{
    return intersectionLambdaPoint(ray, aaRec, true);
  }

  template<AARectangleC AARectangle>
  inline std::vector<typename AARectangle::Point> points(const AARectangle& aaRec) {
    using Point = typename AARectangle::Point;
    Point min = minPoint(aaRec);
    Point max = maxPoint(aaRec);
    u_short fixedDim = fixedDimension(aaRec);

    Point l = min;
    l[(fixedDim +1) % 3] = max[(fixedDim +1) % 3];
    Point m = min;
    m[(fixedDim +2) % 3] = max[(fixedDim +2) % 3];
    return {min, l, max, m};
  }

  template<AARectangleC AARectangle>
  inline std::vector<fp_t> intersectionLambdas(const typename AARectangle::Ray& ray, const AARectangle& aaRec) {

    auto intersectionResult = intersectionLambdaPoint(ray, aaRec);
    if(intersectionResult)
      return { intersectionResult->first };
    return {};
  }

  template<AARectangleC AARectangle>
  inline auto intersectionLambdasInclusive(const typename AARectangle::Ray& ray, const AARectangle& aaRec) -> std::vector<fp_t>{

    auto lambdaPoint = intersectionLambdaPointInclusive(ray, aaRec);
    if(lambdaPoint)
      return { lambdaPoint->first };
    return {};
  }

  template<AARectangleC AARectangle>
  inline bool intersect(const typename AARectangle::LineSegment& ls, const AARectangle& aaRec) {

    auto lambdaPoint = intersectionLambdaPoint(lsRay(ls), aaRec);
    return lambdaPoint && lambdaPoint->first > EPSILON && lambdaPoint->first < 1;
  }

  template<AARectangleC AARectangle>
  inline auto intersectionPointsInclusive(const typename AARectangle::Ray& ray, const AARectangle& aaRec)
                                          -> std::vector<typename AARectangle::Point> {

    auto lambdaPoint = intersectionLambdaPointInclusive(ray, aaRec);
    if(lambdaPoint)
      return { lambdaPoint->second };
    return {};
  }

  template<AARectangleC AARectangle>
  inline auto lineSegments(const AARectangle& aaRec) -> std::vector<typename AARectangle::LineSegment> {
    auto corners = points(aaRec);
    return {{
      { corners[0], corners[1] },
      { corners[1], corners[2] },
      { corners[2], corners[3] },
      { corners[3], corners[0] }
    }};
  }

  template<AARectangleC AARectangle>
  std::ostream& operator<<(std::ostream& o, const AARectangle& aaRec) {
    o << "AARec{" << minPoint(aaRec) << "<" << maxPoint(aaRec) << ", " << fixedDimension(aaRec) << "=" << fixedValue(aaRec) << "}";
    return o;
  }
}