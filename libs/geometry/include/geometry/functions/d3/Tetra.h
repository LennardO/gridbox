//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/interfaces/d3/Concepts.h"

namespace gridbox {

  template<TetraC Tetra>
  inline std::vector<typename Tetra::Facet> facets(const Tetra& tetra) {
    auto pts = points(tetra);

    return {
      {pts[1],pts[2],pts[3]},
      {pts[0],pts[3],pts[2]},
      {pts[0],pts[1],pts[3]},
      {pts[0],pts[2],pts[1]}
    };
  }

  template<TetraC Tetra>
  inline auto lineSegments(const Tetra& tetra) -> std::vector<typename Tetra::LineSegment> {
    auto tetraPoints = points(tetra);
    return {
      {tetraPoints[1], tetraPoints[2]},
      {tetraPoints[2], tetraPoints[3]},
      {tetraPoints[3], tetraPoints[1]},
      {tetraPoints[0], tetraPoints[1]},
      {tetraPoints[3], tetraPoints[0]},
      {tetraPoints[2], tetraPoints[0]}
    };
  }

  template<TetraC Tetra>
  std::ostream& operator<<(std::ostream& o, const Tetra& tetra) {
    auto tetraPoints = points(tetra);
    o << "Tetra{" << tetraPoints[0] << ", " << tetraPoints[1] << ", " << tetraPoints[2] << ", " << tetraPoints[3] << "}";
    return o;
  }

  template<TetraC Tetra>
  inline bool intersect(const AARectanglePolyFacet& aaRec, const Tetra& tetra) {
    using Point = typename Tetra::Point;
    for(auto const& point : points(aaRec))
      if(containsInInterior(tetra, point))
        return true;
    std::set<Point, lex_compare<Point>> iPoints;
    for(auto const& aaRecLS : lineSegments(aaRec))
      for(auto const& iPoint : intersectionPointsInclusive(aaRecLS, tetra))
        iPoints.insert(iPoint);

    if(iPoints.size() >= 2)
      return true;
    iPoints = {};
    for(auto const& tetraLS : lineSegments(tetra))
      for(auto const& iPoint : intersectionPointsInclusive(tetraLS, aaRec))
        iPoints.insert(iPoint);

    return iPoints.size() >= 2;
  }
}