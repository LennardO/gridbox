//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/functions/Vector.h"
#include "geometry/interfaces/d3/Vector3D.h"

namespace gridbox {

  template<Vector3DC Vector3D>
  inline Vector3D crossProduct(const Vector3D& lhs, const Vector3D& rhs) {
    auto lhsCoords = coordinates(lhs);
    auto rhsCoords = coordinates(rhs);
    return {
      lhsCoords[1] * rhsCoords[2] - lhsCoords[2] * rhsCoords[1],
      lhsCoords[2] * rhsCoords[0] - lhsCoords[0] * rhsCoords[2],
      lhsCoords[0] * rhsCoords[1] - lhsCoords[1] * rhsCoords[0]
    };
  }
}