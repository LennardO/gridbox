//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/functions/AABox.h"

namespace gridbox {

  DEFINE_CONCEPT(AABoxByMinMax, AABox);

  template<PointC Point>
  class AABoxByMinMax {
  public:
    using Concept = CAABoxByMinMax;
    inline static const u_int num_orthants = pow2(Point::DIMENSIONS);

    AABoxByMinMax() = default;
    AABoxByMinMax(const Point& min, const Point& max)
    : _min(min)
    , _max(max)
    {}
    AABoxByMinMax(const std::vector<Point>& points)
      : _min(INFINITY)
      , _max(-INFINITY)
    {
      for(auto const& point : points)
        for (u_short d = 0; d < dimensions(point); d++) {
          if (get(point, d) < get(_min, d))
            _min[d] = get(point, d); // todo
          if (get(point, d) > get(_max, d))
            _max[d] = get(point, d);
        }
    }

    Point min() const {
      return _min;
    }
    Point max() const {
      return _max;
    }

  protected:
    Point _min, _max;
  };

  template<AABoxByMinMaxC AABox>
  inline typename AABox::Point minPoint(const AABox& aab) {
    return aab.min();
  }
  template<AABoxByMinMaxC AABox>
  inline typename AABox::Point maxPoint(const AABox& aab) {
    return aab.max();
  }

  template<AABoxByMinMaxC AABox>
  inline AABox aabb(const std::vector<typename AABox::Point>& points) {
    return AABox(points);
  }

//
//  constexpr std::array<std::array<u_short, Point::DIMENSIONS>, NumChilds> zeroOneCombinations() const {
//    std::array<std::array<u_short, Point::DIMENSIONS>, NumChilds> result;
//    for(u_short i=0; i<NumChilds; i++)
//      for(u_short d=0; d<Point::DIMENSIONS; d++)
//        if( ( (i/(u_short)std::pow(2,d)) % 2 ) == 0 )
//          result[i][Point::DIMENSIONS -1 -d] = 0;
//        else
//          result[i][Point::DIMENSIONS -1 -d] = 1;
//
//    return result;
//  }
//  template<AABoxByMinMaxC AABox>
//  inline std::array<AABox, AABox::num_orthants> orthants(const AABox& aab) { //todo: universal implementation
//
//    std::array<Point, 3> minMidMax = {minPoint(aab), midPoint(aab), maxPoint(aab)};
//    std::array<AABox, AABox::num_orthants> result;
//    static const auto combinations = zeroOneCombinations();
//    for(u_short i=0; i<NumChilds; i++) {
//      Point childMin, childMax;
//      for(u_short d=0; d<Point::DIMENSIONS; d++) {
//        childMin[d] = minMidMax[ combinations[i][Point::DIMENSIONS-1-d] ][d];
//        childMax[d] = minMidMax[ combinations[i][Point::DIMENSIONS-1-d]+1 ][d];
//      }
//      result[i] = {childMin, childMax};
//    }
//    return result;
//  }
}