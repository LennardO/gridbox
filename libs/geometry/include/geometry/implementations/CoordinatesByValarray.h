//
// Created by lennard on 6/28/20.
//

#pragma once

#include "geometry/functions/Coordinates.h"

namespace gridbox {

  DEFINE_CONCEPT(CoordinatesByValarray, Coordinates);

  template<u_short D>
  class CoordinatesByValarray {
  public:
    using Concept = CCoordinatesByValarray;
    static const u_short DIMENSIONS = D;

    CoordinatesByValarray()
    : _coordinates(D)
    {}
    CoordinatesByValarray(const std::valarray<fp_t>& coordinates)
    : _coordinates(coordinates)
    {}
    CoordinatesByValarray(const fp_t& value)
    : _coordinates(value, D)
    {}

    std::valarray<fp_t> coordinates() const {
      return _coordinates;
    }

    fp_t operator[](size_t i) const {
      return _coordinates[i];
    }
    fp_t& operator[](size_t i) {
      return _coordinates[i];
    }

  protected:
    std::valarray<fp_t> _coordinates;
  };

  template<CoordinatesByValarrayC Coordinates>
  inline std::valarray<fp_t> coordinates(const Coordinates& coords) {
    return coords.coordinates();
  }

}
