//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/functions/Ray.h"

namespace gridbox {

  DEFINE_CONCEPT(RayWithPrecomputation, Ray);

  template<PointC P>
  class RayWithPrecomputation {
  public:
    using Concept = CRayWithPrecomputation;
    using Point = P;
    using Vector = typename Point::Vector;

    RayWithPrecomputation(const Point& origin, const Vector& direction)
    : _origin(origin)
    , _direction(direction)
    {
      std::valarray<fp_t> midi(0.0, dimensions(origin));
      for(int i=0; i<dimensions(origin); i++)
        if(get(_direction, i) != 0)
          midi[i] = 1.0f / get(_direction, i);
      _mulInvDirection = midi;
    }

    Point origin() const {
      return _origin;
    }
    Vector direction() const {
      return _direction;
    }
    Vector mulInvDirection() const {
      return _mulInvDirection;
    }

  protected:
    Point _origin;
    Vector _direction;
    Vector _mulInvDirection;
  };

  template<RayWithPrecomputationC Ray>
  inline typename Ray::Point origin(const Ray& ray) {
    return ray.origin();
  }
  template<RayWithPrecomputationC Ray>
  inline typename Ray::Vector direction(const Ray& ray) {
    return ray.direction();
  }
  template<RayWithPrecomputationC Ray>
  inline typename Ray::Vector mulInvDirection(const Ray& ray) {
    return ray.mulInvDirection();
  }
}
