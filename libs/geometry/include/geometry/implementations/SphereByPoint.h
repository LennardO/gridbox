//
// Created by lennard on 8/1/20.
//

#pragma once

#include "geometry/interfaces/Sphere.h"

namespace gridbox {

  DEFINE_CONCEPT(SphereByPoint, Sphere);

  template<PointC PointT>
  class SphereByPoint {
  public:
    using Concept = CSphereByPoint;
    using Point = PointT;

    SphereByPoint() = default;
    SphereByPoint(const Point& center, const fp_t& radius)
    : _center(center)
    , _radius(radius)
    {}

    Point center() const {
      return _center;
    }
    [[nodiscard]] fp_t radius() const {
      return _radius;
    }

  private:
    Point _center;
    fp_t _radius = -1;
  };

  template<SphereByPointC Sphere>
  inline typename Sphere::Point center(const Sphere& sphere) {
    return sphere.center();
  }

  template<SphereByPointC Sphere>
  inline fp_t radius(const Sphere& sphere) {
    return sphere.radius();
  }
}