//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/implementations/AABoxByMinMax.h"
#include "geometry/functions/d2/AABox2D.h"
#include "Point2DByValarray.h"
#include "LineSegmentPolyFacet.h"
#include "CirclePyPoint.h"

namespace gridbox {

  DEFINE_CONCEPT(AABox2DByMinMax, AABox2D, AABoxByMinMax);

  class AABox2DByMinMax : public AABoxByMinMax<Point2DByValarray> {
  public:
    using Concept = CAABox2DByMinMax;
    using Point = Point2DByValarray;
    using Vector = Vector2DByValarray;
    using Ray = Ray2DWithPrecomputation;
    using Facet = LineSegmentPolyFacet;
    using Sphere = CircleByPoint;
    using AABoxByMinMax<Point2DByValarray>::AABoxByMinMax;
//    using AABoxByMinMax<Point2DByValarray>::num_orthants;
    static const u_short NumPoints = 4;
    static const u_short NumFacets = 4;
  };

}