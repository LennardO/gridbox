//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/functions/d2/LineSegment2D.h"
#include "Point2DByValarray.h"
#include "Vector2DByValarray.h"
#include "Ray2DWithPrecomputation.h"
#include "geometry/implementations/PointSetByArray.h"

namespace gridbox {

  DEFINE_CONCEPT(LineSegment2DByArray, LineSegment2D, PointSetByArray);

  class LineSegment2DByArray : public PointSetByArray<Point2DByValarray, 2> {
  public:
    using Concept = CLineSegment2DByArray;
    using Ray = Ray2DWithPrecomputation;

    LineSegment2DByArray(const Point& p, const Point& q)
    : PointSetByArray({p, q})
    {}
  };

}