//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/implementations/d2/LineSegment2DByArray.h"
#include "geometry/interfaces/PolyFacet.h"
#include "geometry/interfaces/d2/Concepts.h"
#include "geometry/implementations/d2/Plane2D.h"

namespace gridbox {

  DEFINE_CONCEPT(LineSegmentPolyFacetByArray, LineSegmentPolyFacet, LineSegment2DByArray);

  class LineSegmentPolyFacet : public LineSegment2DByArray {
  public:
    using Concept = CLineSegmentPolyFacetByArray;
    using Plane = Plane2D;
    using Point = Point2DByValarray;

    using LineSegment2DByArray::LineSegment2DByArray;

    LineSegmentPolyFacet(const LineSegment2DByArray& base)
    : LineSegment2DByArray(base)
    {}
  };

  template<LineSegmentPolyFacetByArrayC LSPolyFacet>
  inline typename LSPolyFacet::Plane plane(const LSPolyFacet& facet) {
    return Plane2D(facet);
  }
}