//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/implementations/PlaneByPointAndVector.h"
#include "Point2DByValarray.h"
#include "Vector2DByValarray.h"
#include "LineSegment2DByArray.h"

namespace gridbox {

  DEFINE_CONCEPT(Plane2DByPointAndVector, PlaneByPointAndVector);

  class Plane2D : public PlaneByPointAndVector<Point2DByValarray> {
  public:
    using Concept = CPlane2DByPointAndVector;
    using Point = Point2DByValarray;
    using Vector = Vector2DByValarray;
    using LineSegment = LineSegment2DByArray;

    using PlaneByPointAndVector<Point2DByValarray>::PlaneByPointAndVector;

    Plane2D(const LineSegment& ls)
    : PlaneByPointAndVector(pointP(ls), (pointQ(ls)-pointP(ls)).getTurned90DegreesCW())
    {}
  };
}