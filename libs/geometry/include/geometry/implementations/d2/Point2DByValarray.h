//
// Created by Lennard Ockenfels on 5/27/20.
//

#pragma once

#include "geometry/functions/Point.h"
#include "Vector2DByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(Point2DByValarray, Point, Coordinates2DByValarray);

  class Point2DByValarray : public Coordinates2DByValarray {
  public:
    using Concept = CPoint2DByValarray;
    using Vector = Vector2DByValarray;

    using Coordinates2DByValarray::Coordinates2DByValarray;
  };
}