//
// Created by Lennard Ockenfels on 7/3/20.
//

#pragma once

#include "geometry/functions/d3/AARectangle.h"
#include "Point3DByValarray.h"
#include "LineSegment3DByArray.h"

namespace gridbox {

  DEFINE_CONCEPT(AARectanglePolyFacetImpl, AARectanglePolyFacet);

  class AARectanglePolyFacet {
  public:
    using Concept = CAARectanglePolyFacetImpl;
    using Point = Point3DByValarray;
    using Vector = Vector3DByValarray;
    using Ray = Ray3DWithPrecomputation;
    using LineSegment = LineSegment3DByArray;

    AARectanglePolyFacet(u_short fixedDim, bool fixedIsMin, const Point& min, const Point& max)
    : _fixedDim(fixedDim)
    , _fixedIsMin(fixedIsMin)
    , _min(min)
    , _max(max)
    {}

    u_short fixedDim() const {
      return _fixedDim;
    }
    bool isFixedMin() const {
      return _fixedIsMin;
    }
    const Point& min() const {
      return _min;
    }
    const Point& max() const {
      return _max;
    }

  private:
    u_short _fixedDim;
    bool _fixedIsMin;
    Point _min;
    Point _max;
  };

  template<AARectanglePolyFacetImplC AARecFacet>
  inline typename AARecFacet::Point minPoint(const AARecFacet& aaRec) {
    return aaRec.min();
  }

  template<AARectanglePolyFacetImplC AARecFacet>
  inline typename AARecFacet::Point maxPoint(const AARecFacet& aaRec) {
    return aaRec.max();
  }

  template<AARectanglePolyFacetImplC AARecFacet>
  inline u_short fixedDimension(const AARecFacet& aaRec) {
    return aaRec.fixedDim();
  }

  template<AARectanglePolyFacetImplC AARecFacet>
  inline bool isFixedMin(const AARecFacet& aaRec) {
    return aaRec.isFixedMin();
  }

}