//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/functions/LineSegment.h"
#include "Point3DByValarray.h"
#include "Vector3DByValarray.h"
#include "Ray3DWithPrecomputation.h"
#include "geometry/implementations/PointSetByArray.h"

namespace gridbox {

  DEFINE_CONCEPT(LineSegment3DByArray, LineSegment3D, PointSetByArray);

  class LineSegment3DByArray : public PointSetByArray<Point3DByValarray, 2> {
  public:
    using Concept = CLineSegment3DByArray;
    using Ray = Ray3DWithPrecomputation;

    LineSegment3DByArray(const Point& p, const Point& q)
    : PointSetByArray({p, q})
    {}
  };
}