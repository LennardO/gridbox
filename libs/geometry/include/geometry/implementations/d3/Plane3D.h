//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/implementations/PlaneByPointAndVector.h"
#include "Point3DByValarray.h"
#include "Vector3DByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(Plane3DByPointAndVector, PlaneByPointAndVector);

  class Plane3D : public PlaneByPointAndVector<Point3DByValarray> {
  public:
    using Concept = CPlane3DByPointAndVector;
    using Point = Point3DByValarray;
    using Vector = Vector3DByValarray;

    using PlaneByPointAndVector<Point3DByValarray>::PlaneByPointAndVector;

    Plane3D(const Point& point0CCW, const Point& point1CCW, const Point& point2CCW)
    : PlaneByPointAndVector(point0CCW, crossProduct(point1CCW-point0CCW, point2CCW-point0CCW))
    {}
  };
}