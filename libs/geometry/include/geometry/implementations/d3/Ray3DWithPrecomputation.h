//
// Created by lennard on 6/30/20.
//

#pragma once

#include "geometry/implementations/RayWithPrecomputation.h"
#include "Point3DByValarray.h"

namespace gridbox {
  using Ray3DWithPrecomputation = RayWithPrecomputation<Point3DByValarray>;

  template<>//todo: kann für rays allgemein genutzt werden
  inline Ray3DWithPrecomputation fromJson<Ray3DWithPrecomputation>(const json& rayJSON) {
    auto origin = fromJson<typename Ray3DWithPrecomputation::Point>(rayJSON["origin"]);
    auto direction = fromJson<typename Ray3DWithPrecomputation::Vector>(rayJSON["direction"]);
    return {origin, direction};
  }
}