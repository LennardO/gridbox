//
// Created by lennard on 8/1/20.
//

#pragma once

#include "geometry/implementations/SphereByPoint.h"
#include "Point3DByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(Sphere3D, SphereByPoint);

  class Sphere3D : public SphereByPoint<Point3DByValarray> {
  public:
    using Concept = CSphere3D;
    using SphereByPoint<Point3DByValarray>::SphereByPoint;
  };
}