//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/implementations/PointSetByArray.h"
#include "geometry/functions/ConvexPolytope.h"
#include "geometry/functions/d3/TrianglePolyFacet.h"
#include "geometry/functions/Triangle.h"
#include "Point3DByValarray.h"
#include "LineSegment3DByArray.h"
#include "geometry/implementations/RayWithPrecomputation.h"
#include "AABox3DByMinMax.h"
#include "Plane3D.h"

namespace gridbox {

  DEFINE_CONCEPT(TrianglePolyFacetByArray, TrianglePolyFacet, PointSetByArray);

  class TrianglePolyFacetByArray : public PointSetByArray<Point3DByValarray, 3> {
  public:
    using Concept = CTrianglePolyFacetByArray;
    using LineSegment = LineSegment3DByArray;
    using Plane = Plane3D;
    using Ray = Ray3DWithPrecomputation;

    TrianglePolyFacetByArray(const Point& a, const Point& b, const Point& c)
      : PointSetByArray<Point3DByValarray, 3>::PointSetByArray({a,b,c})
    {}

  };

  template<TrianglePolyFacetByArrayC TrianglePolyFacet>
  inline typename TrianglePolyFacet::Plane plane(const TrianglePolyFacet& triangle) {
    return Plane3D(pointA(triangle), pointB(triangle), pointC(triangle));
  }

}