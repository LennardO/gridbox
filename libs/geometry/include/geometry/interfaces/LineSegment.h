//
// Created by lennard on 6/29/20.
//

#pragma once

#include "AABox.h"

namespace gridbox {

  template<LineSegmentC LineSegment>
  inline typename LineSegment::Point pointP(const LineSegment& ls);
  template<LineSegmentC LineSegment>
  inline typename LineSegment::Point pointQ(const LineSegment& ls);

  template<LineSegmentC LineSegment>
  inline bool parallel(const LineSegment& a, const LineSegment& b);

  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionLambdas(const LineSegment& ls,
                                  const PointSet&    pointSet)
                                  -> std::vector<fp_t>;
  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionLambdasInclusive(const LineSegment& ls,
                                           const PointSet&    pointSet)
                                           -> std::vector<fp_t>;

  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionPoints(const LineSegment& ls,
                                 const PointSet&    pointSet)
                                 -> std::vector<typename PointSet::Point>;
  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionPointsInclusive(const LineSegment& ls,
                                          const PointSet&    pointSet)
                                          -> std::vector<typename PointSet::Point>;

  template<LineSegmentC LineSegment>
  inline auto intersectionLambdasRayLSInclusive(const typename LineSegment::Ray&  ray,
                                                const LineSegment&                ls)
                                                -> std::optional<std::pair<fp_t,fp_t>>;


  template<LineSegmentC LineSegment>
  inline typename LineSegment::Ray lsRay(const LineSegment& ls);
}