//
// Created by lennard on 6/29/20.
//

#pragma once

#include "Point.h"

namespace gridbox {

  template<PlaneC Plane>
  inline typename Plane::Point support(const Plane& plane);
  template<PlaneC Plane>
  inline typename Plane::Vector normal(const Plane& plane);
  template<PlaneC Plane>
  inline bool isLeft(const typename Plane::Point& point, const Plane& plane);
  template<PlaneC Plane>
  inline bool isLeftOrOn(const typename Plane::Point& point, const Plane& plane);

  template<PlaneC Plane>
  std::ostream& operator<<(std::ostream& o, const Plane& plane);
}