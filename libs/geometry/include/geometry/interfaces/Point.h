//
// Created by Lennard Ockenfels on 5/27/20.
//

#pragma once

#include "Vector.h"

namespace gridbox {

  template<PointC Point, VectorC Vector>
  Point operator+(const Point& lhs, const Vector& rhs);
  template<PointC Point, VectorC Vector>
  Vector operator-(const Point& lhs, const Point& rhs);

  template<PointC Point>
  inline Point mid(const Point& p0, const Point& p1);

}