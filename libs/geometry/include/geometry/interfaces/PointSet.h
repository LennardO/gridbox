//
// Created by lennard on 6/29/20.
//

#pragma once

#include "Ray.h"

namespace gridbox {

  template<PointSetC PointSet>
  inline std::vector<typename PointSet::Point> points(const PointSet& pointSet);

  template<PointSetC PointSetA, PointSetC PointSetB>
  inline bool intersect(const PointSetA& pointSetA, const PointSetB& pointSetB);

  template<PointSetC PointSet>
  inline bool intersect(const typename PointSet::Ray& ray, const PointSet& pointSet);
  template<PointSetC PointSet>
  inline bool intersectInclusive(const typename PointSet::Ray& ray, const PointSet& pointSet);

  template<PointSetC PointSet>
  inline auto intersectionLambdas(const typename PointSet::Ray&  ray,
                                  const PointSet&                pointSet)
                                  -> std::vector<fp_t>;
  template<PointSetC PointSet>
  inline auto intersectionLambdasInclusive(const typename PointSet::Ray&  ray,
                                           const PointSet&                pointSet)
                                           -> std::vector<fp_t>;

  template<PointSetC PointSet>
  inline auto intersectionPoints(const typename PointSet::Ray&  ray,
                                 const PointSet&                pointSet)
                                 -> std::vector<typename PointSet::Point>;
  template<PointSetC PointSet>
  inline auto intersectionPointsInclusive(const typename PointSet::Ray&  ray,
                                          const PointSet&                pointSet)
                                          -> std::vector<typename PointSet::Point>;

  template<PointSetC PointSet>
  inline typename PointSet::AABB aabb(const PointSet& pointSet);
  template<PointSetC PointSet>
  inline typename PointSet::AABB aabb(const std::vector<PointSet>& pointSets);

  template<PointSetC PointSet>//todo: require numPoints >= 3 (no LS allowed)
  inline auto lineSegments(const PointSet& pointSet) -> std::vector<typename PointSet::LineSegment>;

  template<PointSetC PointSet>
  inline bool operator==(const PointSet& lhs, const PointSet& rhs);

  template<PointSetC PointSet>
  std::ostream& operator<<(std::ostream& o, const PointSet& pointSet);
}