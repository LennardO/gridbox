//
// Created by lennard on 6/29/20.
//

#pragma once

#include "PointSet.h"

namespace gridbox {

  using FacetID = u_short;
  const FacetID INVALID_FACET_ID = std::numeric_limits<unsigned short>::max();

  template<PolyFacetC PolyFacet>
  inline typename PolyFacet::Plane plane(const PolyFacet& facet);
}