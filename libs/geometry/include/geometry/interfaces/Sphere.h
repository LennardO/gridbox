//
// Created by lennard on 8/1/20.
//

#pragma once

#include "Point.h"

namespace gridbox {

  DEFINE_CONCEPT(Sphere);

  template<SphereC Sphere>
  inline typename Sphere::Point center(const Sphere& sphere);

  template<SphereC Sphere>
  inline fp_t radius(const Sphere& sphere);
}