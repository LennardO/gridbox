//
// Created by lennard on 6/29/20.
//

#pragma once

#include "LineSegment.h"

namespace gridbox {

  template<TriangleC Triangle>
  inline typename Triangle::Point pointA(const Triangle& triangle);
  template<TriangleC Triangle>
  inline typename Triangle::Point pointB(const Triangle& triangle);
  template<TriangleC Triangle>
  inline typename Triangle::Point pointC(const Triangle& triangle);

  template<TriangleC Triangle>
  inline std::vector<typename Triangle::LineSegment> lineSegments(const Triangle& triangle);
}