//
// Created by lennard on 6/24/20.
//

#pragma once

#include "Coordinates.h"

namespace gridbox {

  template<VectorC Vector>
  inline bool parallel(const Vector& a, const Vector& b);
  template<VectorC Vector>
  inline fp_t dotProduct(const Vector& a, const Vector& b);
  template<VectorC Vector>
  inline fp_t norm2(const Vector& a);
  template<VectorC Vector>
  inline Vector random(); //with norm2 == 1

  template<VectorC Vector>
  Vector operator+(const Vector& lhs, const Vector& rhs);
  template<VectorC Vector>
  Vector operator-(const Vector& lhs, const Vector& rhs);
  template<VectorC Vector>
  bool operator<(const Vector& lhs, const Vector& rhs);
  template<VectorC Vector>
  bool operator>(const Vector& lhs, const Vector& rhs);
  template<VectorC Vector>
  Vector operator*(const fp_t& scalar, const Vector& vector);
  template<VectorC Vector>
  Vector operator/(const Vector& vector, const fp_t& scalar);
}