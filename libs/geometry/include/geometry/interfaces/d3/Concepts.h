//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/interfaces/Concepts.h"

namespace gridbox {

  DEFINE_CONCEPT(Vector3D, Vector);
  DEFINE_CONCEPT(LineSegment3D, LineSegment);
  DEFINE_CONCEPT(Tetra, ConvexPolytope);
  DEFINE_CONCEPT(TrianglePolyFacet, Triangle, PolyFacet);
  DEFINE_CONCEPT(AARectangle, PointSet);
  DEFINE_CONCEPT(AARectanglePolyFacet, AARectangle, PolyFacet);
  DEFINE_CONCEPT(AABox3D, AABox);
}