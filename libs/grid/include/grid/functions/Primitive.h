//
// Created by lennard on 7/23/20.
//

#pragma once

#include "grid/interfaces/Primitive.h"
#include "Neighbor.h"

namespace gridbox {

  template<PrimitiveC Primitive>
  std::ostream& operator<<(std::ostream& o, const Primitive& primitive) {
    o << "Primitive{" << (typename Primitive::ConvexPolytope) primitive << ", [";
    for(auto const& nb : neighbors(primitive))
      o << nb;
    o << "] " << data(primitive) << "}";
    return o;
  }

  template<PrimitiveC Primitive>
  inline Primitive fromJson(const json& prtJSON) {
    using Neighbor = typename Primitive::Neighbor;
    auto convexPoly = fromJson<typename Primitive::ConvexPolytope>(prtJSON["convexPolytope"]);
    std::vector<Neighbor> neighbors;
    for(auto const& nb : prtJSON["neighbors"])
      neighbors.emplace_back(fromJson<Neighbor>(nb));
    auto data = prtJSON["data"].get<typename Primitive::Data>();
    return Primitive(convexPoly, neighbors, data);
  }
}