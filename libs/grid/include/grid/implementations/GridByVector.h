//
// Created by lennard on 6/30/20.
//

#pragma once

#include "grid/interfaces/Grid.h"
#include "RegionByVector.h"

namespace gridbox {

  DEFINE_CONCEPT(GridByVector, Grid);

  template<PrimitiveC PrimitiveT>
  class GridByVector {
  public:
    using Concept = CGridByVector;
    using Primitive = PrimitiveT;
    using Region = RegionByVector<Primitive>;

    virtual void loadTestData() = 0;
    virtual void loadFromFile(const std::string& filename) = 0;

    std::vector<Primitive> primitives() const {
      return _primitives;
    }

  protected:
    std::vector<Primitive> _primitives;
  };

  template<GridByVectorC Grid>
  inline void loadTestData(Grid& grid) {
    grid.loadTestData();
  }

  template<GridByVectorC Grid>
  inline void loadFromFile(Grid& grid, const std::string& filename) {
    grid.loadFromFile(filename);
  }

  template<GridByVectorC Grid>
  inline std::vector<typename Grid::Primitive> primitives(const Grid& grid) {
    return grid.primitives();
  }


}