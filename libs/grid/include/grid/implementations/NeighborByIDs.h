//
// Created by lennard on 6/30/20.
//

#pragma once

#include "grid/functions/Neighbor.h"

namespace gridbox {

  DEFINE_CONCEPT(NeighborByIDs, Neighbor);

  class NeighborByIDs {
  public:
    using Concept = CNeighborByIDs;

    NeighborByIDs()
    : _primitiveID(INVALID_PRIMITIVE_ID)
    , _facetID(INVALID_FACET_ID)
    {}
    NeighborByIDs(const PrimitiveID& primitiveID, const FacetID& facetID)
    : _primitiveID(primitiveID)
    , _facetID(facetID)
    {}

    PrimitiveID& primitiveID() {
      return _primitiveID;
    }
    PrimitiveID primitiveID() const {
      return _primitiveID;
    }
    FacetID& facetID() {
      return _facetID;
    }
    FacetID facetID() const {
      return _facetID;
    }

  protected:
    PrimitiveID _primitiveID;
    FacetID _facetID;
  };


  template<NeighborByIDsC Neighbor>
  inline PrimitiveID primitiveID(const Neighbor& neighbor) {
    return neighbor.primitiveID();
  }
  template<NeighborByIDsC Neighbor>
  inline PrimitiveID& primitiveID(Neighbor& neighbor) {
    return neighbor.primitiveID();
  }

  template<NeighborByIDsC Neighbor>
  inline FacetID& facetID(Neighbor& neighbor) {
    return neighbor.facetID();
  }

  template<NeighborByIDsC Neighbor>
  inline FacetID facetID(const Neighbor& neighbor) {
    return neighbor.facetID();
  }

  template<NeighborByIDsC Neighbor>
  inline json toJson(const Neighbor& neighbor) {
    json result = {};
    result["primitiveID"] = primitiveID(neighbor);
    result["facetID"] = facetID(neighbor);
    return result;
  }
}