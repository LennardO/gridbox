//
// Created by lennard on 6/30/20.
//

#pragma once

#include "grid/interfaces/Primitive.h"
#include "NeighborByIDs.h"

namespace gridbox {

  DEFINE_CONCEPT(PrimitiveWithFloat, Primitive);

  template<ConvexPolytopeC ConvexPolytopeT>
  class PrimitiveWithFloat : public ConvexPolytopeT {
  public:
    using ConvexPolytope = ConvexPolytopeT;
    using Neighbor = NeighborByIDs;
    using Data = float;

    PrimitiveWithFloat(const ConvexPolytope& convexPolytope, const std::vector<Neighbor>& neighbors, const Data& data)
    : ConvexPolytope(convexPolytope)
    , _neighbors(neighbors)
    , _data(data)
    {}

    std::vector<Neighbor> neighbors() const {
      return _neighbors;
    }
    Neighbor neighbor(const FacetID& facetID) const {
      return _neighbors[facetID];
    }
    Data data() const {
      return _data;
    }

  protected:
    std::vector<Neighbor> _neighbors;
    Data _data;
  };

  template<PrimitiveWithFloatC Primitive>
  inline std::vector<typename Primitive::Neighbor> neighbors(const Primitive& primitive) {
    return primitive.neighbors();
  }
  template<PrimitiveWithFloatC Primitive>
  inline typename Primitive::Neighbor neighbor(const Primitive& primitive, const FacetID& facetID) {
    return primitive.neighbor(facetID);
  }
  template<PrimitiveWithFloatC Primitive>
  inline typename Primitive::Data data(const Primitive& primitive) {
    return primitive.data();
  }

  template<PrimitiveWithFloatC Primitive>
  inline json toJson(const Primitive& primitive) {
    json result = {};
    result["convexPolytope"] = toJson((typename Primitive::ConvexPolytope) primitive);
    json neighborsJSON = json::array();
    for(auto const& nb : primitive.neighbors())
      neighborsJSON += toJson(nb);
    result["neighbors"] = neighborsJSON;
    result["data"] = data(primitive);
    return result;
  }

}