//
// Created by lennard on 6/30/20.
//

#pragma once

#include "grid/implementations/GridByVector.h"
#include "TetraPrimitive.h"

#include <OpenEXR/half.h>

namespace gridbox {

  class TetraGrid : public GridByVector<TetraPrimitive> {
  public:
    using Ray = Ray3DWithPrecomputation;
    using Point = typename TetraPrimitive::Point;
    using AABox = typename TetraPrimitive::AABB;
    using Neighbor = typename TetraPrimitive::Neighbor;

    void loadTestData() override {
      PrimitiveID ff = INVALID_PRIMITIVE_ID;
      FacetID f = INVALID_FACET_ID;

      Point swBack  = {0,0,0};
      Point seBack  = {1,0,0};
      Point neBack  = {1,1,0};
      Point nwBack  = {0,1,0};
      Point swFront = {0,0,1};
      Point seFront = {1,0,1};
      Point neFront = {1,1,1};
      Point nwFront = {0,1,1};

      _primitives =
        {{//                  points                           faces {pID, fID}          data
          { { nwBack,  swFront, swBack,  seBack },  {{ {ff,f},{ff,f},{ 4,2},{ff,f} }}, 471.1 }, //  0 back lower/left
          { { nwBack,  neFront, seBack,  neBack },  {{ {ff,f},{ff,f},{ff,f},{ 4,3} }}, 471.1 }, //  1 back upper/right
          { { nwBack,  nwFront, swFront, neFront }, {{ {ff,f},{ 4,1},{ff,f},{ff,f} }}, 471.1 }, //  2 front upper/left
          { { seFront, neFront, seBack,  swFront }, {{ { 4,0},{ff,f},{ff,f},{ff,f} }}, 471.1 }, //  3 front lower/right
          { { nwBack,  seBack,  neFront, swFront }, {{ { 3,0},{ 2,1},{ 0,2},{ 1,3} }}, 471.1 }  //  4 mid
        }};
    }

    void loadFromFile(const std::string& filename) override {
      std::ifstream is(filename, std::ios::binary);
      if (is.fail())
        throw std::runtime_error("could not open file " + filename);

      unsigned long long i = 0;
      unsigned int numEdgePrimitives;

      is.read(reinterpret_cast<char*>(&numEdgePrimitives), sizeof(numEdgePrimitives));
      LOG(DEBUG) << "num_edge_tetras: " << numEdgePrimitives;
      is.ignore(15 * sizeof(unsigned int));

      while (true) {
        std::array<Point,4> points;

        std::array<std::array<fp_t, 3>, 4> pointsBuffer;
        is.read(reinterpret_cast<char*>(&pointsBuffer), sizeof(pointsBuffer));

        for(int i=0; i<4; i++)
          points[i] = std::valarray<fp_t>(pointsBuffer[i].data(), 3);

        std::array<u_int32_t, 4> payload{};
        is.read(reinterpret_cast<char*>(&payload), sizeof(payload));

        std::vector<Neighbor> neighbors(4);
        primitiveID(neighbors[0]) = 0x00FFFFFF & (payload[0] >> 8) ;
        primitiveID(neighbors[1]) = (0x00FF0000 & (payload[0] << 16)) | (0x0000FFFF & (payload[1] >> 16)) ;
        primitiveID(neighbors[2]) = (0x00FFFF00 & (payload[1] << 8)) | (0x000000FF & (payload[2] >> 24)) ;
        primitiveID(neighbors[3]) = 0x00FFFFFF & payload[2] ;

        facetID(neighbors[0]) = 0x00000003 & (payload[3] >> 30) ;
        facetID(neighbors[1]) = 0x00000003 & (payload[3] >> 28) ;
        facetID(neighbors[2]) = 0x00000003 & (payload[3] >> 26) ;
        facetID(neighbors[3]) = 0x00000003 & (payload[3] >> 24) ;

//        neighbors[0].setNeighborID(0x00FFFFFF & (payload[0] >> 8) );
//        neighbors[1].setNeighborID((0x00FF0000 & (payload[0] << 16)) | (0x0000FFFF & (payload[1] >> 16)) );
//        neighbors[2].setNeighborID((0x00FFFF00 & (payload[1] << 8)) | (0x000000FF & (payload[2] >> 24)) );
//        neighbors[3].setNeighborID(0x00FFFFFF & payload[2] );
//
//        neighbors[0].setNeighborFaceID(0x00000003 & (payload[3] >> 30) );
//        neighbors[1].setNeighborFaceID(0x00000003 & (payload[3] >> 28) );
//        neighbors[2].setNeighborFaceID(0x00000003 & (payload[3] >> 26) );
//        neighbors[3].setNeighborFaceID(0x00000003 & (payload[3] >> 24) );

        half h;
        h.setBits(0x0000FFFF & payload[3]);

        if (!is.eof()) {
          TetraByPointArray tetra(points);
          TetraPrimitive tetraPrimitive(tetra, neighbors, (fp_t) h);
          _primitives.emplace_back(tetraPrimitive);

          ++i;
        }
        else
          break;
      }

      LOG(INFO) << "Read " << i << " tetras from disk." << std::endl;
    }
  };
}