//
// Created by lennard on 6/30/20.
//

#pragma once

#include "geometry/interfaces/PolyFacet.h"

namespace gridbox {

  using PrimitiveID = int;
  const PrimitiveID INVALID_PRIMITIVE_ID = 0xFFFFFF;

  DEFINE_CONCEPT(Neighbor);

  template<NeighborC Neighbor>
  inline PrimitiveID& primitiveID(Neighbor& neighbor);

  template<NeighborC Neighbor>
  inline FacetID& facetID(Neighbor& neighbor);

  template<NeighborC Neighbor>
  inline bool isValid(const Neighbor& neighbor);

  template<NeighborC Neighbor>
  std::ostream& operator<<(std::ostream& o, const Neighbor& neighbor);
}