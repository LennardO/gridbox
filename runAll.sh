#!/usr/bin/env bash

mrsArray=('80' '100' '140' '180' '240' '320' '420' '540' '680' '840' '1020' '1220' '1440' '1680' '1940' '2220' '2520' '2840')

for mrs in "${mrsArray[@]}"
do
  echo running construction and query for maxRegionSize=${mrs}
  ./cmake-build-release/bin/construction-bm data/models/AhmedBody/AhmedBody_2_1MioCells_Var_p_cropped_ranked_header_LinkedMini-2.bin ~/AhmedResults/benchmark ~/AhmedResults/AhmedOctree${mrs}.json ${mrs}
  ./cmake-build-release/bin/query-bm ~/AhmedResults/AhmedOctree${mrs}.json ~/AhmedResults/AhmedTestRays-100-10.json ~/AhmedResults/benchmark
done
