//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"

class Point3DTest : public TestFigure3D {
protected:

  Point a = {0,1,2};
  Point b = {2,3,4};
  Point abMid = {1,2,3};
  Vector vFromAtoB = {2,2,2};
};

TEST_F(Point3DTest, mid) {
  ASSERT_EQ(gridbox::mid(a,b), abMid);
  ASSERT_EQ(gridbox::mid(a,a), a);
}

TEST_F(Point3DTest, plus) {
  ASSERT_EQ(a + vFromAtoB, b);
}

TEST_F(Point3DTest, minus) {
  ASSERT_EQ(b - a, vFromAtoB);
}