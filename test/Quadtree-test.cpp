//
// Created by lennard on 7/6/20.
//

#include <algorithms/implementations/d2/LinearTriangleGridSearch.h>
#include "TestFigure2D.h"

#include "algorithms/implementations/d2/QuadtreeTriangleGridSearch.h"
#include "algorithms/functions/RegionSearch.h"

class QuadtreeTest : public TestFigure2D {
protected:
  LinearTriangleGridSearch lgs;
  QuadtreeTriangleGridSearch qgs;
  Ray ray = {{1, -0.5}, {1, 0.8}};

  void SetUp() override {
    TestFigure2D::SetUp();
    TriangleGrid tg;
    loadTestData(tg);
    construct(qgs, tg, 4);
    construct(lgs, tg, 4);
  }
};

TEST_F(QuadtreeTest, getIntersectedBy) {
  auto result = getIntersectedBy(qgs, ray);
//  ASSERT_EQ(intersection_counter<CAABox>(), 21);
  ASSERT_EQ(RAY_INTERSECT_COUNT(AABox), 21);
  std::valarray<typename Region::size_type> expected = {2,3,3,4,4,4,2};
  ASSERT_TRUE((regionSizes(result) == expected).min());

  LOG(DEBUG) << intersectedPrimitives(result, ray).size();
  auto lgsResult = getIntersectedBy(lgs, ray);
  ASSERT_TRUE(isOrderedSubset(lgsResult, result));
}
