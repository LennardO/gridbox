//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"

class Ray3DTest : public TestFigure3D {
protected:

  Point rayOrigin = {1,-2,0};
  Vector rayDirection = {1,2,-4};
  Point rayPointAt2 = {3,2,-8};
  Vector mulInvDir = {1,0.5,-0.25};
  Ray ray = {rayOrigin, rayDirection};
};

TEST_F(Ray3DTest, origin) {
  ASSERT_EQ(origin(ray), rayOrigin);
}

TEST_F(Ray3DTest, direction) {
  ASSERT_EQ(direction(ray), rayDirection);
}

TEST_F(Ray3DTest, mulInvDirection) {
  ASSERT_EQ(mulInvDirection(ray), mulInvDir);
}

TEST_F(Ray3DTest, rayPoint) {
  ASSERT_EQ(rayPoint(ray, 0), origin(ray));
  ASSERT_EQ(rayPoint(ray, 1), origin(ray)+direction(ray));
  ASSERT_EQ(rayPoint(ray, 0.5), mid(origin(ray),rayPoint(ray,1)));
  ASSERT_EQ(rayPoint(ray, 2), rayPointAt2);
}