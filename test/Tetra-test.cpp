//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"

class TetraTest : public TestFigure3D {
protected:

  Point back = {1,1,1};
  Point front = {1,1,3};
  Point right = {3,1,2};
  Point top = {2,3,2};
  Tetra tetra = {top, front, back, right};
  
  Point pointInside = {2,2,2};
  Point pointInsideOffset = {2,1.3,1.9};
  Point pointOnLeftSide = {1.5,2,2};
  Point pointOutside = {2,4,2};

  Ray intersectsFacets = {{0,1,2}, {1,0.5,0}};
  Ray intersectsThroughCorner = {{0,0,0}, {1,1,1}};
  Ray intersectsLeftSideColinear = {{1.5,2,0}, {0,0,1}};
  Ray touchesCorner = {{0,0,0}, {2,3,2}};
  Ray touchesSide = {{0,2,2}, {1,-1,0}};
  Ray startsOnSideNotCrossing = {{1.5, 2, 2}, {-1, 1, 1}};
  Ray startsOnSideCrossing = {{1.5, 2, 2}, {1, 0, 1}};
  Ray notIntersecting = {{1, 1, 0}, {1, 1, 1}};

  AABox containsTetra = {{0,0,0},{4,4,4}};
  AABox cornerInsideTetra = {{2,1.5,0}, {4,4,1.9}};
//  AABox cornerInsideTetra = {{2,2,0}, {4,4,2}};
  AABox containsOneCorner = {{0,2,0},{4,4,4}};
  AABox pureFacetIntersection = {{0,2,0},{4,2.5,4}};
  AABox touchesRightCorner = {{3,0,0},{4,4,4}};
  AABox cornerTouchesLS = {{2,1,2.5},{4,4,4}};
  AABox lsTouch = {{2,0,2.5},{4,4,4}};

  Tetra tetra2 = {{1,0,1}, {1,1,1}, back, right};
};

TEST_F(TetraTest, contains) {
  ASSERT_TRUE(contains(tetra, pointInside));
  ASSERT_TRUE(contains(tetra, pointOnLeftSide));
  ASSERT_TRUE(contains(tetra, top));
  ASSERT_FALSE(contains(tetra, pointOutside));
}

TEST_F(TetraTest, containsInInterior) {
  ASSERT_TRUE(containsInInterior(tetra, pointInside));
  ASSERT_TRUE(containsInInterior(tetra, pointInsideOffset));
  ASSERT_FALSE(containsInInterior(tetra, pointOnLeftSide));
  ASSERT_FALSE(containsInInterior(tetra, front));
  ASSERT_FALSE(containsInInterior(tetra, pointOutside));
}

TEST_F(TetraTest, aabb) {

  auto boundingBox = aabb( tetra );
  Point expectedMin = { 1,1,1 };
  Point expectedMax = { 3,3,3 };
  ASSERT_EQ(minPoint(boundingBox), expectedMin);
  ASSERT_EQ(maxPoint(boundingBox), expectedMax);
}

TEST_F(TetraTest, intersectRay) {

  ASSERT_TRUE(intersect(intersectsFacets, tetra));
  ASSERT_TRUE(intersect(intersectsThroughCorner, tetra));
  ASSERT_TRUE(intersect(intersectsLeftSideColinear, tetra));
  ASSERT_FALSE(intersect(touchesCorner, tetra));
  ASSERT_FALSE(intersect(touchesSide, tetra));
  ASSERT_FALSE(intersect(startsOnSideNotCrossing, tetra));
  ASSERT_TRUE(intersect(startsOnSideCrossing, tetra));
  ASSERT_FALSE(intersect(notIntersecting, tetra));
}

TEST_F(TetraTest, intersectInclusiveRay) {

  ASSERT_TRUE(intersectInclusive(intersectsFacets, tetra));
  ASSERT_TRUE(intersectInclusive(intersectsThroughCorner, tetra));
  ASSERT_TRUE(intersectInclusive(intersectsLeftSideColinear, tetra));
  ASSERT_TRUE(intersectInclusive(touchesCorner, tetra));
  ASSERT_TRUE(intersectInclusive(touchesSide, tetra));
  ASSERT_TRUE(intersectInclusive(startsOnSideNotCrossing, tetra));
  ASSERT_TRUE(intersectInclusive(startsOnSideCrossing, tetra));
  ASSERT_FALSE(intersectInclusive(notIntersecting, tetra));
}

TEST_F(TetraTest, intersectAABox) {
  ASSERT_TRUE(intersect(tetra, containsTetra));
  ASSERT_TRUE(intersect(tetra, cornerInsideTetra));
  ASSERT_TRUE(intersect(tetra, containsOneCorner));
  ASSERT_TRUE(intersect(tetra, pureFacetIntersection));
  ASSERT_FALSE(intersect(tetra, touchesRightCorner));
  ASSERT_FALSE(intersect(tetra, cornerTouchesLS));
  ASSERT_FALSE(intersect(tetra, lsTouch));
}

